package edu.buffalo.cse116;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.awt.*;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import edu.buffalo.cse116.Cards;
import edu.buffalo.cse116.PlayerSelect.PlayerClickListener;

public class suggestionGUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel window;
	private JPanel l;
	private JPanel r;
	private JPanel m;
	private JPanel bot;
	private JComboBox<String> susp;
	private JComboBox<String> room;
	private JComboBox<String> weap;
	private JButton suggest;
	public List<String> Susp;
	public List<String> Weap;
	public List<String> Room;
	public static ArrayList<String> SuggestedCards;
	
	String[] _susp = {"Miss Scarlett","Col. Mustard","Mrs. White","Rev. Green","Ms. Peacock","Prof. Plum"};
	String[] _weap = {"Candlestick","Rope","Knife", "Lead Pipe","Pistol","Wrench"};
	String[] _room = {"Kitchen","Ballroom","Conservatory","Billiard Room","Library","Study","Hall","Lounge","Dining Room"};

	private Game game;
	private Player x;
	
	 suggestionGUI(Player player) 
	 {
		 setLocation(600,50);
		 window = new JPanel(new BorderLayout(20,20));

		 l = new JPanel();
		 m = new JPanel();
		 r = new JPanel();

		 window.setBorder(BorderFactory.createTitledBorder("Please make your suggestion"));
		 window.add(l, BorderLayout.PAGE_START);
		 window.add(r, BorderLayout.CENTER);
		 window.add(m, BorderLayout.PAGE_END);
		 sugButton();
		 window.setPreferredSize(getPreferredSize());
		 window.add(new DimensionsPane(), BorderLayout.CENTER);
		 
		 add(window);
		 
		 setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	     pack();
	     setVisible(true);
	 }
	 
	 public void sugButton()
	 {
		Susp = new ArrayList<String>(Arrays.asList(_susp));  // Adds all the Strings from Array to ArrayList
		Weap = new ArrayList<String>(Arrays.asList(_weap));
		Room = new ArrayList<String>(Arrays.asList(_room));
	 }
	 
	    public class DimensionsPane extends JPanel {

	        /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public DimensionsPane() {
	            setLayout(new GridBagLayout());
	            GridBagConstraints gbc = new GridBagConstraints();
	            gbc.gridx = 1;
	            gbc.gridy = 0;
	            gbc.fill = 1;
	            gbc.weightx = 3;
	            gbc.weighty = 2;
	            gbc.anchor = GridBagConstraints.WEST;
	            add(susp = new JComboBox<String>(_susp), gbc);
	            gbc.gridy+=2;
	            add(weap = new JComboBox<String>(_weap), gbc);
	            gbc.gridy+=2;
	            add(room = new JComboBox<String>(_room), gbc);
	            gbc.gridy+=2;
	            add(suggest = new JButton("suggest"), gbc);
	            suggest.addActionListener(new SuggestClickListener(suggest));
	        }

	    }
	    
	    public Dimension getPreferredSize()
	    {
	    	return new Dimension(300,200);
	    }
	 
	 public class SuggestClickListener implements ActionListener 
	 {
		 private JButton _b;
	    	public SuggestClickListener(JButton b) 
	    	{
	    		_b = b;
	    	}
	    	
			public void actionPerformed(ActionEvent e) 
			{
				// TODO Auto-generated method stub
				ArrayList<String> SuggestedCards = new ArrayList<String>();
				if(SuggestedCards.isEmpty() == false)
				{
					SuggestedCards.clear();
				}
				
				String s = Susp.get(susp.getSelectedIndex());
				String w = Weap.get(weap.getSelectedIndex());
				String r = String.valueOf((Game.players.getLast()._CurrentSpace));
				SuggestedCards.add(s);
				SuggestedCards.add(w);
				SuggestedCards.add(r);
				System.out.println(SuggestedCards);
				_b.setEnabled(false);
				Game.suggestionAnswer(Game.players,Game.players.getLast(), SuggestedCards);
			}
			
	 }
}
