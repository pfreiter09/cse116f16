package edu.buffalo.cse116;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;


public class CardsTest extends Cards {
		
		Cards test = new Cards();
		@SuppressWarnings("static-access")
		@Test
		public void cardsTest()
		{
			Cards test = new Cards();
			ArrayList<String> e = new ArrayList<String>();
			e.add("T"+"B"+"C");
			e.add("Q");
			assertTrue(test.randInt(e) >= 0);
			assertTrue(test.randInt(e) <= 1);
			assertFalse(test.randInt(e) > 2);
			
		}
		@Test
		public void creatPlayerstest()
		{
			LinkedList<Player> playerTest = test.createPlayers(3);
			assertEquals("Tests Creates Correct Size", 3, playerTest.size());
		}
		@Test
		public void createPlayersTest2()
		{
			LinkedList<Player> playerTest = test.createPlayers(0);
			assertEquals("Test with 0 players",0, playerTest.size());
			
		}
		@Test
		public void shuffleCardsTest()
		{
			System.out.println(Suspects);
			List<String> shuffled = test.shuffleCards(Suspects);
			System.out.println(shuffled.size());
			assertEquals(6, shuffled.size(), 0.0001);
			List<String> shufflesWeapons = test.shuffleCards(Weapons);
			assertEquals(6, shufflesWeapons.size(), 0.0001);
			List<String> shufflesRooms = test.shuffleCards(Rooms);
			assertEquals(9, shufflesRooms.size(),0.0001);	
		}
		@Test
		public void centerCasetest()
		{
			List<String> centerTest = new ArrayList<String>(test.centerCase(test.Deck));
			List<String> suspects = new ArrayList<String>(test.getSuspects());
			List<String> weapons = new ArrayList<String>(test.getWeapons());
			List<String> room = new ArrayList<String>(test.getRooms());
			System.out.println(centerTest.size());
			assertEquals(3, centerTest.size());
			System.out.print(suspects);
			assertTrue(suspects.contains(centerTest.get(0)));
			assertTrue(weapons.contains(centerTest.get(1)));
			assertTrue(room.contains(centerTest.get(2)));
		}
		
		@Test
		public void shuffleToDealtest()
		{
			List<String> shuffleTest = new ArrayList<String>(test.shuffleToDeal());
			List<String> unshuffledDeck = new ArrayList<String>();
			unshuffledDeck.addAll(test.Suspects);
			unshuffledDeck.addAll(test.Weapons);
			unshuffledDeck.addAll(test.Rooms);
			assertEquals(21, shuffleTest.size());
			assertNotEquals(shuffleTest.get(0),unshuffledDeck.get(0));
		}

		@Test
		public void playerDealTest()
		{
			LinkedList<Player> thePlayers = new LinkedList<Player>();
			thePlayers = test.createPlayers(2);
			ArrayList<String> deck = new ArrayList<String>(test.shuffleToDeal());
			test.playerDeal(thePlayers, deck);
			assertFalse(thePlayers.get(0).cardsInHand.isEmpty());
			assertFalse(thePlayers.get(1).cardsInHand.isEmpty());
			assertTrue(test.Deck.isEmpty());
		}
		
}
