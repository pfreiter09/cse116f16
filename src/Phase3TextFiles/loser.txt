To begin run the program in the Driver class and select any number of players
and choose the character for each player. (note that the console has printed the solution)

Once completed the Board GUI opens up and in the bottom left should several buttons,
one of which is to make an accusation. .

Select this button and choose the player, weapon, and room from the drop down
windows in any combination so long as they do NOT correspond 
to the solution printed in the console and select submit.

A message should display stating that the accusation is incorrect and they have lost the game. 
(note that we had the console print the hand of the player that lost to more easily conduct the last test)

The game will continue as before but the losing player(s) will be 
prevented from taking turns. 
(select the end turn button to progress through the taking turns rotation).

If a suggestion is made with the card(s) that were in a losing players hand, the suggestion 
will still work properly and it will display that the losing player holds that card.
(use the suggestion button in the same fashion that the accusation button was used).
 


