package edu.buffalo.cse116;

import java.awt.Color;
import java.awt.Container;
import java.awt.Font;

import javax.swing.*;
import javax.swing.border.BevelBorder;

public class NumberPlayerGUI extends javax.swing.JFrame {
		/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	  private javax.swing.JButton jButton1;
	    private javax.swing.JButton jButton2;
	    private javax.swing.JButton jButton3;
	    private javax.swing.JButton jButton4;
	    private javax.swing.JLabel jLabel1;
	    private javax.swing.JPanel jPanel1;
		
		
	public NumberPlayerGUI(){
		
		makeTheThings();
	}
	public void makeTheThings(){
		 

		        jPanel1 = new javax.swing.JPanel();
		        jLabel1 = new javax.swing.JLabel();
		        jButton1 = new javax.swing.JButton();
		        jButton2 = new javax.swing.JButton();
		        jButton3 = new javax.swing.JButton();
		        jButton4 = new javax.swing.JButton();

		        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		        setTitle("Cluedo");
		        setLocation(600,400);

		        jLabel1.setText("How Many People Are Playing?");

		        jButton1.setText("3 Players");
		       

		        jButton2.setText("4 Players");

		        jButton3.setText("5 Players");

		        jButton4.setText("6 Players");
		        
		        jButton1.addActionListener(new java.awt.event.ActionListener() {
		            public void actionPerformed(java.awt.event.ActionEvent evt) {
		                jButton1ActionPerformed(evt);
		            }
		        });
		        
		        jButton2.addActionListener(new java.awt.event.ActionListener() {
		            public void actionPerformed(java.awt.event.ActionEvent evt) {
		                jButton2ActionPerformed(evt);
		            }
		        });

		        jButton3.addActionListener(new java.awt.event.ActionListener() {
		            public void actionPerformed(java.awt.event.ActionEvent evt) {
		                jButton3ActionPerformed(evt);
		            }
		        });
		        	
		        jButton4.addActionListener(new java.awt.event.ActionListener() {
		            public void actionPerformed(java.awt.event.ActionEvent evt) {
		                jButton4ActionPerformed(evt);
		            }
		        });
		       
		        
		        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
		        jPanel1.setLayout(jPanel1Layout);
		        jPanel1Layout.setHorizontalGroup(
		            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		            .addGroup(jPanel1Layout.createSequentialGroup()
		                .addContainerGap()
		                .addComponent(jLabel1)
		                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		            .addGroup(jPanel1Layout.createSequentialGroup()
		                .addComponent(jButton1)
		                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
		                .addComponent(jButton2)
		                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
		                .addComponent(jButton3)
		                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
		                .addComponent(jButton4)
		                .addGap(0, 0, Short.MAX_VALUE))
		        );
		        jPanel1Layout.setVerticalGroup(
		            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		            .addGroup(jPanel1Layout.createSequentialGroup()
		                .addContainerGap()
		                .addComponent(jLabel1)
		                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
		                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
		                    .addComponent(jButton1)
		                    .addComponent(jButton2)
		                    .addComponent(jButton3)
		                    .addComponent(jButton4))
		                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		        );

		        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		        getContentPane().setLayout(layout);
		        layout.setHorizontalGroup(
		            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
		        );
		        layout.setVerticalGroup(
		            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
		            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
		                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
		                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
		                .addGap(1, 1, 1))
		        );

		        pack();
		        setVisible(true);
		    }              
	
	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {      
		new Game(3);
		setVisible(false);
	}
	private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {      
		new Game(4);
		setVisible(false);
	}
	
	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {      
		new Game(5);
		setVisible(false);
		
}
	private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {      
		new Game(6);
		setVisible(false);
		
	}
	
}
