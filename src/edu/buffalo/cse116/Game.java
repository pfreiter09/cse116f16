package edu.buffalo.cse116;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

import javax.swing.JButton;

import edu.buffalo.cse116.Cards;

public class Game 
{
	int checkedIndex;
	/**The list of legal space numbers returned by the assignLegalSpaces method */
	ArrayList<Integer> legalSpaces = new ArrayList<Integer>();
	/** The Scanner that reads user input*/
	Scanner reader = new Scanner(System.in);
	//Object of the Cards class to be used by the Game class.
	static Cards deck = new Cards();
	//List of the players participating in the game.
	public static LinkedList<Player> players = new LinkedList<Player>();
	//MasterList of inital players, mainly used for cards
	public static LinkedList<Player> playersPlaying = new LinkedList<Player>();
	//List of the cards being suggested by a player
	public static ArrayList<String> suggestedCards = new ArrayList<String>();
	
	public static ArrayList<String> loseAccuseCards = new ArrayList<String>();
	
	
	//the amount of spaces in a move for a player determined by a die roll.
	int spacesToMove = 0;
	

	
	/**
	 * Default game constructor.
	 */
	public Game()
	{
		
	}
	/**
	 * Game constructor that will run the game properly based on a specific number
	 * of players.
	 * @param numbPlayers
	 */
	public Game(int numbPlayers)
	{
		new PlayerSelect(numbPlayers);
		createWhoDidIt();
		deck.shuffleToDeal(); 
//		createPlayerHands(players);
		/* for(int i = 0; i < players.size(); i++)
		{
			players.get(i).setStartingPosition();
		}
		while(true)
		{
			for(int i = 0; i < players.size(); i++)
			{
				takeTurn(players.get(i));
			}
		}
		*/
	}


/**
 * Picks three cards of each different type randomly.
 * Makes list of left overcards(To deal out to players
 */
	public void createWhoDidIt()
	{
		deck.centerCase(deck.Deck);
	}
	public static Player beginTurn()
	{
		return players.peekFirst();
	}
	public static void endTurn()
	{
		Player p = players.pollFirst();
		players.offer(p);
		
	}
	public static void loseAccuse() throws IOException
	{
		//loseAccuseCards.addAll(players.peekFirst().cardsInHand);
		System.out.println(players.peekFirst() + "'s cards: " + players.peekFirst().cardsInHand);
		players.removeFirst();
		if(players.size()>0)
		{
		new GameBoardGUI(players.peekFirst());
		new LostGUI();
		}
		else
			new WinnerGUI();
	}
	/**
	 * Deals the remaining cards to the players that are participating in the game.
	 * @param a the list of players.
	 */
	public static void createPlayerHands(LinkedList<Player> a)
	{
		deck.playerDeal(a, deck.Deck);
	}
	/**
	 * Turn taking sequence. If they start in a corner room you can use a 
	 * secret passage to another room and make a suggestion. Prompt player to use
	 * secret passage or not..
	 * if they don't start in a corner room they make a move.
	 * @param x player taking turn
	 */
	public HashSet<String> canPlayerDeny(Player p){
		//ArrayList<Player> sug = new ArrayList<Player>(players);
		HashSet<String> hasCard = new HashSet<String>();
		for(int i =0; i < suggestedCards.size();i++)
		{
			
				if(p.cardsInHand.contains(suggestedCards.get(i)))
				{
					hasCard.add(suggestedCards.get(i));
				}
			
		}
		return hasCard;
		
	}
	
	public boolean canPlayersDeny()
	{
		for (int i = 1; i < players.size(); i++) {
			HashSet<String> result = canPlayerDeny(players.get(i));
			if (result.size() > 0) {
				//we know we have a match, so we can break out of the loop.
				//open the dialogue for refuting cards
				break;
			}
		}
		return false;
		
	}
	
	public LinkedList<Player> getPlayerCards(){
		return playersPlaying;
	}
	public void takeTurn(Player x)
	{
		System.out.println(x._CharacterUsed + ", it is your turn.");
		if(isSpaceRoom(x.getCurrentSpace()))
		{
			//Accusation method
		}
		if (isCornerRoom(x.getCurrentSpace()))
		{
			if(doYouWantToUsePassage())
			{
				useSecretPassage(x._CurrentSpace, x);
				//Suggestion
			}					
		}
		else
		{ 
			//Move(players.pop(x), );
			if(isSpaceRoom(x.getCurrentSpace()))
			{
				
			}	
		}
	}
	/**Allows for a player to choose a character to suggest.
	 * 
	 * @param x the player that is making the suggestion
	 * @param choice the number of the card that they would like to suggest
	 * (will eventually become user input but was done like this for testing purposes)
	 * @return true or false depending on legal input.
	 */
	public boolean suggestedCharacterCard(int choice) {
		
		if (choice == 0) {
			suggestedCards.set(0, "Col. Mustard");
			return true;
		}
		if (choice == 1) {
			suggestedCards.set(0, "Miss Scarlett");
			return true;
		}
		if (choice == 2) {
			suggestedCards.set(0, "Prof. Plum");
			return true;
		}
		if (choice == 3) {
			suggestedCards.set(0, "Rev. Green");
			return true;
		}
		if (choice == 4) {
			suggestedCards.set(0, "Mrs. White");
			return true;
		}
		if (choice == 5) {
			suggestedCards.set(0, "Ms. Peacock");
			return true;
		}
		else
		{
			return false;
		}
	}
	/**Allows for a player to choose a weapon to suggest.
	 * 
	 * @param x the player that is making the suggestion
	 * @param choice the number of the card that they would like to suggest
	 * (will eventually become user input but was done like this for testing purposes)
	 * @return true or false depending on legal input.
	 */
	public boolean suggestedWeaponCard(int Choice) {
		
		if (Choice == 0) {
			suggestedCards.set(1, "Candlestick");
			return true;
		}
		if (Choice == 1) {
			suggestedCards.set(1, "Rope");
			return true;
		}
		if (Choice == 2) {
			suggestedCards.set(1, "Knife");
			return true;
		}
		if (Choice == 3) {
			suggestedCards.set(1, "Lead Pipe");
			return true;
		}
		if (Choice == 4) {
			suggestedCards.set(1, "Pistol");
			return true;
		}
		if (Choice == 5) {
			suggestedCards.set(1, "Wrench");
			return true;
		}
		else {
			return false;
		}
	}
	/**Allows for a player to choose a room to suggest.
	 * 
	 * @param x the player that is making the suggestion
	 * @param choice the number of the card that they would like to suggest
	 * (will eventually become user input but was done like this for testing purposes)
	 * @return true or false depending on legal input.
	 */
	public boolean suggestedRoomCard(int Choice) {
		
		if (Choice == 0) {
			suggestedCards.set(2, "Kitchen");
			return true;
		}
		if (Choice == 1) {
			suggestedCards.set(2, "Ballroom");
			return true;
		}
		if (Choice == 2) {
			suggestedCards.set(2, "Conservatory");
			return true;
		}
		if (Choice == 3) {
			suggestedCards.set(2, "Billiard Room");
			return true;
		}
		if (Choice == 4) {
			suggestedCards.set(2, "Library");
			return true;
		}
		if (Choice == 5) {
			suggestedCards.set(2, "Study");
			return true;
		}
		if (Choice == 6) {
			suggestedCards.set(2, "Hall");
			return true;
		}
		if (Choice == 7) {
			suggestedCards.set(2, "Lounge");
			return true;
		}
		if (Choice == 8) {
			suggestedCards.set(2, "Dining Room");
			return true;
		}
		else {
			return false;
		}
	}
	public static boolean suggestionAnswer( Player x)
	{
		
		
		LinkedList<Player> orderPlayers = new LinkedList<Player>();
		orderPlayers = playersPlaying;
		orderPlayers.remove(x);
		//int playerSlot = playersPlaying.indexOf(x);
		System.out.println("You suggested: " + suggestedCards);
//		while(playersPlaying.isEmpty() ==  false)
//		{
//			if(playerSlot == playersPlaying.size())
//			{
//				playerSlot = 0;
//			}
//			orderPlayers.add(playersPlaying.get(playerSlot));
//			playersPlaying.remove();
//		}	
	
		for(int i = 0; i < orderPlayers.size(); i++)
		{
			//System.out.println(orderPlayers);
			//System.out.println(suggestedCards);
			for(int j = 0; j < suggestedCards.size(); j++ )
			{	
				
				
				if(orderPlayers.get(i).cardsInHand.contains(suggestedCards.get(j)))
				{
					
					System.out.println(orderPlayers.get(i)._CharacterUsed + " revealed that they have the " + suggestedCards.get(j) + " card!");
					
					return true;
					
				}
//				if(loseAccuseCards.contains(suggestedCards.get(j))){
//					System.out.println("a character that lost revealed that they have the " + suggestedCards.get(j) + " card!");
//					return true;
//				}
			}
			
		}
		System.out.println("Nobody can prove your suggestion false!");
		return false;
	}

	/** CODE IRRELEVANT TO STAGE 1
	 * 
	//defines suggested room
	public void roomSuggestion(Player x) {
		int suggestedRoom = x.getCurrentSpace();
	}
	
	//suggest a player where "pawn" will be the corresponding character piece on the board
	public void playerSuggestion(pawn a, pawn b, pawn c, pawn d, pawn e, pawn f) {
		String p = null;
		System.out.println("Suggest a Player: Mustard, Scarlet, Plum, Green, White, or Peacock.");
		p = reader.next();
		if (p.equalsIgnoreCase("mustard")) {
			suggestedPlayer = a;
		}
		if (p.equalsIgnoreCase("scarlet")) {
			suggestedPlayer = b;
		}
		if (p.equalsIgnoreCase("plum")) {
			suggestedPlayer = c;
		}
		if (p.equalsIgnoreCase("green")) {
			suggestedPlayer = d;
		}
		if (p.equalsIgnoreCase("white")) {
			suggestedPlayer = e;
		}
		if (p.equalsIgnoreCase("peacock")) {
			suggestedPlayer = f;
		}
	}
	
	//suggest a weapon where "weap" will be the corresponding weapon piece on the board
	public void suggestedWeapon(weap a, weap b, weap c, weap d, weap e, weap f) {
		String w = null;
		System.out.println("Suggest a Weapon: Candlestick, Rope, Knife, Lead Pipe, Pistol, or Wrench");
		w = reader.next();
		if (Choice == ("candlestick")) {
			suggestedWeapon = a; 
		}
		if (Choice == ("rope")) {
			suggestedWeapon = b;
		}
		if (Choice == ("knife")) {
			suggestedWeapon = c;
		}
		if (Choice == ("lead pipe")) { 
			suggestedWeapon = d;
		}
		if (Choice == ("pistol")) {
			suggestedWeapon = e;
		}
		if (Choice == ("wrench")) {
			suggestedWeapon = f;
		}
	}
	
	//moves suggested player/weapon to suggested room
	public void suggestionMade(suggestedRoom, suggestedPlayer, suggestedWeapon){
		if (suggestedRoom == 189) {
			suggestedPlayer.setCurrentSpace(189);
			suggestedWeapon.setCurrentSpace(189);
			return true;
	}
		if (suggestedRoom == 190) {
			suggestedPlayer.setCurrentSpace(190);
			suggestedWeapon.setCurrentSpace(190);
			return true;
	}
		if (suggestedRoom == 191) {
			suggestedPlayer.setCurrentSpace(191);
			suggestedWeapon.setCurrentSpace(191);
			return true;
	}
		if (suggestedRoom == 192) {
			suggestedPlayer.setCurrentSpace(192);
			suggestedWeapon.setCurrentSpace(192);
			return true;
	}
		if (suggestedRoom == 193) {
			suggestedPlayer.setCurrentSpace(193);
			suggestedWeapon.setCurrentSpace(193);
			return true;
	}
		if (suggestedRoom == 194) {
			suggestedPlayer.setCurrentSpace(194);
			suggestedWeapon.setCurrentSpace(194);
			return true;
	}
		if (suggestedRoom == 195) {
			suggestedPlayer.setCurrentSpace(195);
			suggestedWeapon.setCurrentSpace(195);
			return true;
	}
		if (suggestedRoom == 196) {
			suggestedPlayer.setCurrentSpace(196);
			suggestedWeapon.setCurrentSpace(196);
			return true;
	}
		if (suggestedRoom == 197) {
			suggestedPlayer.setCurrentSpace(197);
			suggestedWeapon.setCurrentSpace(197);
			return true;
	}
}
		
	*/
		
	
	
	/**
	 * * Asks player if they want to use the passage(y/n)
	 * if yes, return true, if no, return false, if neither input again
	 * 
	 * @return true or false depending on the player's input.
	 */
	
	
	public boolean doYouWantToUsePassage(){
		System.out.println("You're in a corner room, would you like to use the secret passage? (y or n)");
		
		while(!reader.hasNext("[ynYN]"))
		{
			System.out.println("Please enter y or n");
			reader.next();
		}
		String s = reader.next();
		if(s.equalsIgnoreCase("y"))
		{
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	
	/**
	 * Changes current space of player to connected room via secret passage.
	 * @param i. current room player is in
	 * @param x player making move
	 * @return exits if loops
	 */
		public boolean useSecretPassage(int i, Player x) {
		if( i == 189){
			x.setCurrentSpace(193);
			return true;
			
		}if(i == 193){
			x.setCurrentSpace(189);
			return true;
		}
		if(i == 191){
			x.setCurrentSpace(196);
			return true;
		}
		if(i == 196){
			x.setCurrentSpace(191);
			return true;
		}
		
		return false;
	}
		/** 
		 * Chooses random number between 1-6
		 * @return number between 1-6
		 */
		public static int diceRoll(){
			return (int) ((Math.random()*6) + 1);
		}
/**
 * Creates random number(1-6) for number of moves. Has player input space to move to, 
 * Checks if space selected is a legal move. Continues intill out of moves. 
 * If they enter into a room. Move ends
 * @param x. Player making move( allows to access current space of player
 * @return false = turn is completed true: player ends in a room;
 */
		public boolean Move(Player x, JButton btn)
		{
			
			 spacesToMove = diceRoll();
			int i = 0;
			while( i < spacesToMove){
				int spaceSelected = spaceUserSelected();
				if(assignLegalSpaces(Integer.parseInt(btn.getText())).contains(spaceSelected))
				{
					x.setCurrentSpace(spaceSelected);
					if(isSpaceRoom(spaceSelected))
					{
						return true;
					}
					i = i+1;
					System.out.println("You have" +(spacesToMove - i)+ "moves left");
				}
				else
				{
					System.out.println("BRAH THATS NOT LEGAL TRY AGAIN");
					
				}
			}
			System.out.println("You have no moves left");
			return false;	
		}
		 
		/**
		 * Gets input from User. If number is not a valid space( 1-195)
		 * Prompts user in till valid space number.
		 * @return int space user selected
		 */
		public int spaceUserSelected()
		{
			System.out.println("Choose a space # to move to");
			int s = reader.nextInt();
			while( s > 197)
			{
				System.out.println( s+  " is not a valid space number, must be 1-195. Enter valid number.");
				s = reader.nextInt();
			}
			return s;
		}
		
		/**
		 * Checks if a space is a corner room on the board.
		 * @param spaceOn. Current space a player is on
		 * @return true if space is corner room
		 */
			public boolean isCornerRoom(int spaceOn)
			{
				ArrayList<Integer> cornerNumbers = new ArrayList<Integer>();
				cornerNumbers.add(189);
				cornerNumbers.add(191);
				cornerNumbers.add(193);
				cornerNumbers.add(196);
				if(cornerNumbers.contains(spaceOn)){
					return true;
				}
				else{
					return false;
				}
				
			}
			
			
			/**
			 * creats list of spaces that are rooms. Checks if space is a room
			 * @param spaceSelected: Space in question
			 * @return true if it is a room
			 */
			public boolean isSpaceRoom(int spaceSelected)
			{
				ArrayList<Integer> _roomNumbers = new ArrayList<Integer>();
				_roomNumbers.add(189);
				_roomNumbers.add(190);
				_roomNumbers.add(191);
				_roomNumbers.add(192);
				_roomNumbers.add(193);
				_roomNumbers.add(194);
				_roomNumbers.add(195);
				_roomNumbers.add(196);
				_roomNumbers.add(197);
				if(_roomNumbers.contains(spaceSelected)){
					return true;
				}
				else{
					return false;
				}
			}
			/**
			 * Move method without user input for testing purposes.
			 * @param x Player who is moving.
			 * @param whereAreYou the space the player is on
			 * @param whereYouWannaGo the space the player would like to go to
			 * @return true or false depending on legality of move.
			 */
			public boolean moveTo(Player x, int whereAreYou, int whereYouWannaGo)
			{
				if(assignLegalSpaces(whereAreYou).contains(whereYouWannaGo))
				{
					x.setCurrentSpace(whereYouWannaGo);
					if(isSpaceRoom(whereYouWannaGo))
					{
						return true;
					}
					else
					{
						return true;
					}
				}
				return false;
			}
		/**
		 * Method that assigns to an ArrayList the legal space numbers that a player can
		 * move to based on the space number they are currently on
		 * @param i the number of the space they are on
		 * @return the list of space numbers they can move to.
		 */
			public ArrayList<Integer> assignLegalSpaces(int i)
			{
				checkedIndex = i;
				switch(checkedIndex)
				{
				case 1:
					legalSpaces.add(3);
					break;
				case 2:
					legalSpaces.add(6);
					break;
				case 3:
					legalSpaces.add(1);
					legalSpaces.add(4);
					legalSpaces.add(7);
					break;
				case 4:
					legalSpaces.add(3);
					legalSpaces.add(8);
					break;
				case 5:
					legalSpaces.add(6);
					legalSpaces.add(9);
					break;
				case 6:
					legalSpaces.add(2);
					legalSpaces.add(5);
					legalSpaces.add(10);
					break;
				case 7:
					legalSpaces.add(3);
					legalSpaces.add(8);
					legalSpaces.add(11);
					break;
				case 8:
					legalSpaces.add(4);
					legalSpaces.add(7);
					legalSpaces.add(12);
					break;
				case 9:
					legalSpaces.add(5);
					legalSpaces.add(10);
					legalSpaces.add(13);
					break;
				case 10:
					legalSpaces.add(6);
					legalSpaces.add(9);
					legalSpaces.add(14);
					break;
				case 11:
					legalSpaces.add(7);
					legalSpaces.add(12);
					legalSpaces.add(21);
					break;
				case 12:
					legalSpaces.add(8);
					legalSpaces.add(11);
					legalSpaces.add(22);
					break;
				case 13:
					legalSpaces.add(9);
					legalSpaces.add(14);
					legalSpaces.add(23);
					break;
				case 14:
					legalSpaces.add(10);
					legalSpaces.add(14);
					legalSpaces.add(24);
					break;
				case 15:
					legalSpaces.add(16);
					legalSpaces.add(26);
					break;
				case 16:
					legalSpaces.add(15);
					legalSpaces.add(17);
					legalSpaces.add(27);
					break;
				case 17:
					legalSpaces.add(16);
					legalSpaces.add(18);
					legalSpaces.add(28);
					break;
				case 18:
					legalSpaces.add(17);
					legalSpaces.add(19);
					legalSpaces.add(29);
					break;
				case 19:
					legalSpaces.add(18);
					legalSpaces.add(20);
					legalSpaces.add(30);
					break;
				case 20:
					legalSpaces.add(19);
					legalSpaces.add(21);
					legalSpaces.add(31);
					legalSpaces.add(196);
					break;
				case 21:
					legalSpaces.add(11);
					legalSpaces.add(20);
					legalSpaces.add(22);
					legalSpaces.add(32);
					break;
				case 22:
					legalSpaces.add(12);
					legalSpaces.add(21);
					legalSpaces.add(33);
					legalSpaces.add(197);
					break;
				case 23:
					legalSpaces.add(13);
					legalSpaces.add(24);
					legalSpaces.add(34);
					break;
				case 24:
					legalSpaces.add(14);
					legalSpaces.add(23);
					legalSpaces.add(35);
					break;
				case 25:
					legalSpaces.add(26);
					break;
				case 26:
					legalSpaces.add(15);
					legalSpaces.add(25);
					legalSpaces.add(27);
					break;
				case 27:
					legalSpaces.add(16);
					legalSpaces.add(26);
					legalSpaces.add(28);
					break;
				case 28:
					legalSpaces.add(17);
					legalSpaces.add(27);
					legalSpaces.add(29);
					break;
				case 29:
					legalSpaces.add(18);
					legalSpaces.add(28);
					legalSpaces.add(30);
					break;
				case 30:
					legalSpaces.add(19);
					legalSpaces.add(29);
					legalSpaces.add(31);
					break;
				case 31:
					legalSpaces.add(20);
					legalSpaces.add(30);
					legalSpaces.add(32);
					legalSpaces.add(36);
					break;
				case 32:
					legalSpaces.add(21);
					legalSpaces.add(31);
					legalSpaces.add(33);
					legalSpaces.add(37);
					break;
				case 33:
					legalSpaces.add(22);
					legalSpaces.add(32);
					legalSpaces.add(38);
					break;
				case 34:
					legalSpaces.add(23);
					legalSpaces.add(35);
					legalSpaces.add(39);
					break;
				case 35:
					legalSpaces.add(24);
					legalSpaces.add(34);
					legalSpaces.add(40);
					break;
				case 36:
					legalSpaces.add(31);
					legalSpaces.add(37);
					break;
				case 37:
					legalSpaces.add(32);
					legalSpaces.add(36);
					legalSpaces.add(38);
					legalSpaces.add(47);
					break;
				case 38:
					legalSpaces.add(33);
					legalSpaces.add(37);
					legalSpaces.add(48);
					break;
				case 39:
					legalSpaces.add(34);
					legalSpaces.add(40);
					legalSpaces.add(55);
					break;
				case 40:
					legalSpaces.add(35);
					legalSpaces.add(39);
					legalSpaces.add(41);
					legalSpaces.add(56);
					break;
				case 41:
					legalSpaces.add(40);
					legalSpaces.add(42);
					legalSpaces.add(57);
					legalSpaces.add(189);
					break;
				case 42:
					legalSpaces.add(41);
					legalSpaces.add(43);
					legalSpaces.add(58);
					break;
				case 43:
					legalSpaces.add(42);
					legalSpaces.add(44);
					legalSpaces.add(59);
					break;
				case 44:
					legalSpaces.add(43);
					legalSpaces.add(45);
					legalSpaces.add(60);
					break;
				case 45:
					legalSpaces.add(44);
					legalSpaces.add(46);
					legalSpaces.add(61);
				case 46:
					legalSpaces.add(45);
					legalSpaces.add(62);
					break;
				case 47:
					legalSpaces.add(37);
					legalSpaces.add(48);
					legalSpaces.add(64);
					break;
				case 48:
					legalSpaces.add(38);
					legalSpaces.add(47);
					legalSpaces.add(49);
					legalSpaces.add(65);
					break;
				case 49:
					legalSpaces.add(48);
					legalSpaces.add(50);
					break;
				case 50:
					legalSpaces.add(49);
					legalSpaces.add(51);
					break;
				case 51:
					legalSpaces.add(50);
					legalSpaces.add(52);
					legalSpaces.add(197);
					break;
				case 52:
					legalSpaces.add(51);
					legalSpaces.add(53);
					legalSpaces.add(197);
					break;
				case 53:
					legalSpaces.add(52);
					legalSpaces.add(54);
					break;
				case 54:
					legalSpaces.add(53);
					legalSpaces.add(55);
					legalSpaces.add(66);
					break;
				case 55:
					legalSpaces.add(39);
					legalSpaces.add(54);
					legalSpaces.add(56);
					legalSpaces.add(67);
					break;
				case 56:
					legalSpaces.add(40);
					legalSpaces.add(55);
					legalSpaces.add(57);
					legalSpaces.add(68);
					break;
				case 57:
					legalSpaces.add(41);
					legalSpaces.add(56);
					legalSpaces.add(58);
					legalSpaces.add(69);
					break;
				case 58:
				legalSpaces.add(42);
					legalSpaces.add(57);
					legalSpaces.add(59);
					legalSpaces.add(70);
					break;
				case 59:
					legalSpaces.add(43);
					legalSpaces.add(58);
					legalSpaces.add(60);
					legalSpaces.add(71);
					break;
				case 60:
					legalSpaces.add(44);
					legalSpaces.add(59);
					legalSpaces.add(61);
					legalSpaces.add(72);
					break;
				case 61:
					legalSpaces.add(45);
					legalSpaces.add(60);
					legalSpaces.add(62);
					legalSpaces.add(73);
					break;
				case 62:
					legalSpaces.add(46);
					legalSpaces.add(61);
					legalSpaces.add(63);
					legalSpaces.add(74);
					break;
				case 63:
					legalSpaces.add(62);
					break;
				case 64:
					legalSpaces.add(47);
					legalSpaces.add(65);
					legalSpaces.add(75);
					legalSpaces.add(195);
					break;
				case 65:
					legalSpaces.add(48);
					legalSpaces.add(64);
					legalSpaces.add(76);
					break;
				case 66:
					legalSpaces.add(54);
					legalSpaces.add(67);
					legalSpaces.add(77);
				case 67:
					legalSpaces.add(55);
					legalSpaces.add(66);
					legalSpaces.add(68);
					legalSpaces.add(78);
				case 68:
					legalSpaces.add(56);
					legalSpaces.add(67);
					legalSpaces.add(69);
					break;
				case 69:
					legalSpaces.add(57);
					legalSpaces.add(68);
					legalSpaces.add(70);
					legalSpaces.add(190);
					break;
			   case 70:
				   	legalSpaces.add(58);
					legalSpaces.add(69);
					legalSpaces.add(71);
					break;
				case 71:
					legalSpaces.add(59);
					legalSpaces.add(70);
					legalSpaces.add(72);
					break;
				case 72:
					legalSpaces.add(60);
					legalSpaces.add(71);
					legalSpaces.add(73);
					break;
				case 73:
					legalSpaces.add(61);
					legalSpaces.add(72);
					legalSpaces.add(74);
					break;
				case 74:
					legalSpaces.add(58);
					legalSpaces.add(73);
					break;
				case 75:
					legalSpaces.add(64);
					legalSpaces.add(76);
					legalSpaces.add(80);
					break;
				case 76:
					legalSpaces.add(65);
					legalSpaces.add(75);
					legalSpaces.add(81);
					break;
				case 77:
					legalSpaces.add(66);
					legalSpaces.add(78);
					legalSpaces.add(82);
					break;
				case 78:
					legalSpaces.add(67);
					legalSpaces.add(77);
					legalSpaces.add(83);
					break;
				case 79:
					legalSpaces.add(80);
					legalSpaces.add(89);
					break;
				case 80:
					legalSpaces.add(75);
					legalSpaces.add(79);
					legalSpaces.add(81);
					legalSpaces.add(90);
					break;
				case 81:
					legalSpaces.add(76);
					legalSpaces.add(80);
					legalSpaces.add(91);
					break;
				case 82:
					legalSpaces.add(77);
					legalSpaces.add(83);
					legalSpaces.add(92);
					break;
				case 83:
					legalSpaces.add(78);
					legalSpaces.add(82);
					legalSpaces.add(93);
					break;
				case 84:
					legalSpaces.add(85);
					legalSpaces.add(194);
					break;
				case 85:
					legalSpaces.add(84);
					legalSpaces.add(86);
					break;
				case 86:
					legalSpaces.add(85);
					legalSpaces.add(87);
					legalSpaces.add(195);
					break;
				case 87:
					legalSpaces.add(86);
					legalSpaces.add(88);
					break;
				case 88:
					legalSpaces.add(87);
					legalSpaces.add(89);
					break;
				case 89:
					legalSpaces.add(79);
					legalSpaces.add(88);
					legalSpaces.add(90);
					legalSpaces.add(94);
					break;
				case 90:
					legalSpaces.add(80);
					legalSpaces.add(89);
					legalSpaces.add(91);
					legalSpaces.add(95);
					break;
				case 91:
					legalSpaces.add(81);
					legalSpaces.add(90);
					legalSpaces.add(96);
					break;
				case 92:
					legalSpaces.add(82);
					legalSpaces.add(93);
					legalSpaces.add(97);
					break;
				case 93:
					legalSpaces.add(83);
					legalSpaces.add(92);
					legalSpaces.add(98);
					break;
				case 94:
					legalSpaces.add(89);
					legalSpaces.add(95);
					legalSpaces.add(99);
					break;
				case 95:
					legalSpaces.add(90);
					legalSpaces.add(94);
					legalSpaces.add(96);
					legalSpaces.add(100);
					break;
				case 96:
					legalSpaces.add(91);
					legalSpaces.add(95);
					legalSpaces.add(101);
					break;
				case 97:
					legalSpaces.add(92);
					legalSpaces.add(98);
					legalSpaces.add(102);
					break;
				case 98:
					legalSpaces.add(93);
					legalSpaces.add(97);
					legalSpaces.add(103);
					legalSpaces.add(190);
					break;
				case 99:
					legalSpaces.add(94);
					legalSpaces.add(100);
					legalSpaces.add(104);
					break;
				case 100:
					legalSpaces.add(95);
					legalSpaces.add(99);
					legalSpaces.add(101);
					legalSpaces.add(105);
					break;
				case 101:
					legalSpaces.add(96);
					legalSpaces.add(100);
					legalSpaces.add(106);
					break;
				case 102:
					legalSpaces.add(97);
					legalSpaces.add(103);
					legalSpaces.add(107);
					break;
				case 103:
					legalSpaces.add(98);
					legalSpaces.add(102);
					legalSpaces.add(108);
					break;
				case 104:
					legalSpaces.add(99);
					legalSpaces.add(105);
					legalSpaces.add(109);
					break;
				case 105:
					legalSpaces.add(100);
					legalSpaces.add(104);
					legalSpaces.add(106);
					legalSpaces.add(110);
					break;
				case 106:
					legalSpaces.add(101);
					legalSpaces.add(105);
					legalSpaces.add(111);
					break;
				case 107:
					legalSpaces.add(102);
					legalSpaces.add(108);
					legalSpaces.add(117);
					break;
				case 108:
					legalSpaces.add(103);
					legalSpaces.add(107);
					legalSpaces.add(118);
					break;
				case 109:
					legalSpaces.add(104);
					legalSpaces.add(110);
					legalSpaces.add(122);
					legalSpaces.add(194);
				case 110:
					legalSpaces.add(105);
					legalSpaces.add(109);
					legalSpaces.add(111);
					legalSpaces.add(123);	
					break;
				case 111:
					legalSpaces.add(106);
					legalSpaces.add(110);
					legalSpaces.add(112);
					legalSpaces.add(124);
					break;
				case 112:
					legalSpaces.add(111);
					legalSpaces.add(113);
					legalSpaces.add(125);
					break;
				case 113:
					legalSpaces.add(112);
					legalSpaces.add(114);
					legalSpaces.add(126);
					break;
				case 114:
					legalSpaces.add(113);
					legalSpaces.add(115);
					legalSpaces.add(127);
					break;
				case 115:
					legalSpaces.add(114);
					legalSpaces.add(116);
					legalSpaces.add(128);
					break;
				case 116:
					legalSpaces.add(115);
					legalSpaces.add(117);
					legalSpaces.add(129);
					break;
				case 117:
					legalSpaces.add(107);
					legalSpaces.add(116);
					legalSpaces.add(118);
					legalSpaces.add(130);
					break;
				case 118:
					legalSpaces.add(108);
					legalSpaces.add(117);
					legalSpaces.add(119);
					legalSpaces.add(131);
					break;
				case 119:
					legalSpaces.add(118);
					legalSpaces.add(120);
					legalSpaces.add(132);
					break;
				case 120:
					legalSpaces.add(119);
					legalSpaces.add(121);
					legalSpaces.add(133);
					break;
				case 121:
					legalSpaces.add(120);
					legalSpaces.add(134);
					break;
				case 122:
					legalSpaces.add(109);
					legalSpaces.add(123);
					legalSpaces.add(144);
					break;
				case 123:
					legalSpaces.add(110);
					legalSpaces.add(122);
					legalSpaces.add(124);
					legalSpaces.add(145);
					break;
				case 124:
					legalSpaces.add(111);
					legalSpaces.add(123);
					legalSpaces.add(125);
					break;
				case 125:
					legalSpaces.add(112);
					legalSpaces.add(124);
					legalSpaces.add(126);
					legalSpaces.add(192);
					break;
				case 126:
					legalSpaces.add(113);
					legalSpaces.add(125);
					legalSpaces.add(127);
					break;
				case 127:
					legalSpaces.add(114);
					legalSpaces.add(126);
					legalSpaces.add(128);
					break;
				case 128:
					legalSpaces.add(115);
					legalSpaces.add(127);
					legalSpaces.add(129);
					break;
				case 129:
					legalSpaces.add(116);
					legalSpaces.add(128);
					legalSpaces.add(130);
					break;
				case 130:
					legalSpaces.add(117);
					legalSpaces.add(129);
					legalSpaces.add(131);
					legalSpaces.add(192);
					break;
				case 131:
					legalSpaces.add(118);
					legalSpaces.add(130);
					legalSpaces.add(132);
					break;
				case 132:
					legalSpaces.add(119);
					legalSpaces.add(131);
					legalSpaces.add(133);
					legalSpaces.add(146);
					break;
				case 133:
					legalSpaces.add(120);
					legalSpaces.add(132);
					legalSpaces.add(134);
					legalSpaces.add(147);
					break;
				case 134:
					legalSpaces.add(121);
					legalSpaces.add(133);
					legalSpaces.add(135);
					legalSpaces.add(148);
					break;
				case 135:
					legalSpaces.add(134);
					legalSpaces.add(136);
					legalSpaces.add(149);
					break;
				case 136:
					legalSpaces.add(135);
					legalSpaces.add(137);
					legalSpaces.add(150);
					break;
				case 137:
					legalSpaces.add(136);
					legalSpaces.add(138);
					legalSpaces.add(151);
					break;
				case 138:
					legalSpaces.add(137);
					legalSpaces.add(152);
					break;
				case 139:
					legalSpaces.add(140);
					legalSpaces.add(155);
					break;
				case 140:
					legalSpaces.add(139);
					legalSpaces.add(141);
					legalSpaces.add(156);
					break;
				case 141:
					legalSpaces.add(140);
					legalSpaces.add(142);
					legalSpaces.add(157);
					break;
				case 142:
					legalSpaces.add(141);
					legalSpaces.add(143);
					legalSpaces.add(158);
					break;
				case 143:
					legalSpaces.add(142);
					legalSpaces.add(144);
					legalSpaces.add(159);
					break;
				case 144:
					legalSpaces.add(122);
					legalSpaces.add(143);
					legalSpaces.add(145);
					legalSpaces.add(160);
					break;
				case 145:
					legalSpaces.add(123);
					legalSpaces.add(144);
					legalSpaces.add(161);
					break;
				case 146:
					legalSpaces.add(132);
					legalSpaces.add(147);
					legalSpaces.add(162);
					break;	
				case 147:
					legalSpaces.add(133);
					legalSpaces.add(146);
					legalSpaces.add(148);
					legalSpaces.add(163);
					break;
				case 148:
					legalSpaces.add(134);
					legalSpaces.add(147);
					legalSpaces.add(149);
					break;
				case 149:
					legalSpaces.add(135);
					legalSpaces.add(148);
					legalSpaces.add(150);
					legalSpaces.add(191);
					break;
				case 150:
					legalSpaces.add(136);
					legalSpaces.add(149);
					legalSpaces.add(151);
					break;
				case 151:
					legalSpaces.add(137);
					legalSpaces.add(150);
					legalSpaces.add(152);
					break;
				case 152:
					legalSpaces.add(138);
					legalSpaces.add(151);
					legalSpaces.add(153);
					break;
				case 153:
					legalSpaces.add(152);
					break;
				case 154:
					legalSpaces.add(155);
					break;
				case 155:
					legalSpaces.add(139);
					legalSpaces.add(154);
					legalSpaces.add(156);
					break;
				case 156:
					legalSpaces.add(140);
					legalSpaces.add(155);
					legalSpaces.add(157);
					break;
				case 157:
					legalSpaces.add(141);
					legalSpaces.add(156);
					legalSpaces.add(158);
					break;
				case 158:
					legalSpaces.add(142);
					legalSpaces.add(157);
					legalSpaces.add(159);
					break;
				case 159:
					legalSpaces.add(143);
					legalSpaces.add(158);
					legalSpaces.add(160);
					legalSpaces.add(164);
					break;
				case 160:
					legalSpaces.add(144);
					legalSpaces.add(159);
					legalSpaces.add(161);
					legalSpaces.add(165);
					break;
				case 161:
					legalSpaces.add(145);
					legalSpaces.add(160);
					legalSpaces.add(166);
					break;
				case 162:
					legalSpaces.add(146);
					legalSpaces.add(163);
					legalSpaces.add(167);
					break;
				case 163:
					legalSpaces.add(147);
					legalSpaces.add(162);
					legalSpaces.add(168);
					break;
				case 164:
					legalSpaces.add(159);
					legalSpaces.add(165);
					legalSpaces.add(193);
					break;
				case 165:
					legalSpaces.add(160);
					legalSpaces.add(164);
					legalSpaces.add(166);
					legalSpaces.add(169);
					break;
				case 166:
					legalSpaces.add(161);
					legalSpaces.add(165);
					legalSpaces.add(170);
					legalSpaces.add(192);
					break;
				case 167:
					legalSpaces.add(162);
					legalSpaces.add(168);
					legalSpaces.add(171);
					legalSpaces.add(192);
					break;
				case 168:
					legalSpaces.add(163);
					legalSpaces.add(167);
					legalSpaces.add(172);
					break;
				case 169:
					legalSpaces.add(165);
					legalSpaces.add(170);
					legalSpaces.add(173);
					break;
				case 170:
					legalSpaces.add(166);
					legalSpaces.add(169);
					legalSpaces.add(174);
					break;
				case 171:
					legalSpaces.add(167);
					legalSpaces.add(172);
					legalSpaces.add(175);
					break;
				case 172:
					legalSpaces.add(168);
					legalSpaces.add(171);
					legalSpaces.add(176);
					break;
				case 173:
					legalSpaces.add(169);
					legalSpaces.add(174);
					legalSpaces.add(177);
					break;
				case 174:
					legalSpaces.add(170);
					legalSpaces.add(173);
					legalSpaces.add(178);
					break;
				case 175:
					legalSpaces.add(171);
					legalSpaces.add(176);
					legalSpaces.add(179);
					break;
				case 176:
					legalSpaces.add(172);
					legalSpaces.add(175);
					legalSpaces.add(180);
					break;
				case 177:
					legalSpaces.add(173);
					legalSpaces.add(178);
					break;
				case 178:
					legalSpaces.add(174);
					legalSpaces.add(177);
					legalSpaces.add(181);
					break;
				case 179:
					legalSpaces.add(175);
					legalSpaces.add(180);
					legalSpaces.add(186);
					break;
				case 180:
					legalSpaces.add(176);
					legalSpaces.add(179);
					break;
				case 181:
					legalSpaces.add(178);
					legalSpaces.add(182);
					break;
				case 182:
					legalSpaces.add(181);
					legalSpaces.add(183);
					break;
				case 183:
					legalSpaces.add(182);
					legalSpaces.add(187);
					break;
				case 184:
					legalSpaces.add(185);
					legalSpaces.add(188);
					break;
				case 185:
					legalSpaces.add(184);
					legalSpaces.add(186);
					break;
				case 186:
					legalSpaces.add(179);
					legalSpaces.add(185);
					break;
				case 187:
					legalSpaces.add(183);
					break;
				case 188:
					legalSpaces.add(184);
					break;
				case 189:
					legalSpaces.add(41);
					break;
				case 190:
					legalSpaces.add(69);
					legalSpaces.add(98);
					break;
				case 191:
					legalSpaces.add(149);
					break;
				case 192:
					legalSpaces.add(125);
					legalSpaces.add(130);
					legalSpaces.add(166);
					legalSpaces.add(167);
					break;
				case 193:
					legalSpaces.add(164);
					break;
				case 194:
					legalSpaces.add(84);
					legalSpaces.add(109);
					break;
				case 195:
					legalSpaces.add(64);
					legalSpaces.add(86);
					break;
				case 196:
					legalSpaces.add(20);
					break;
				case 197:
					legalSpaces.add(22);
					legalSpaces.add(51);
					legalSpaces.add(52);
					break;
				}
				return legalSpaces;
			}
		/*
		 * Only use for testing purposes
		 */
		public void setDiceRoll(int i){
			spacesToMove = i;
		}
}
		
			
			




