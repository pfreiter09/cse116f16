package edu.buffalo.cse116;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GameBoardGUI extends javax.swing.JFrame
{
	private JPanel window;
	private JButton[][] b;
	private int index;

	Integer[] array = {3,5,2,6,8};
	
	GameBoardGUI()
	{
		
		this.setSize(1280, 720);
		JButtons();
		this.add(window);
//		window.remove(9);
//		window.remove(24);
//		window.remove(10);
        setVisible(true);
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
		
		
		
	}
	
	public void JButtons()
	{
		window = new JPanel();
		GridLayout layout = new GridLayout(10,10);
		layout.setHgap(1);
		layout.setVgap(1);
		window.setLayout(layout);
		b = new JButton[10][10];
		for(int i = 0; i<10;i++)
		{
			for(int j = 0; j<10;j++)
			{
				window.add(b[i][j] = new JButton(""+(i*10+j)));
				b[i][j].addActionListener(new SuggestClickListener());
			}
		}
	}
	public boolean Walls(int x)
	{
//		index = x;
		for(int i = 0; i<10 ; i++)
		{
			for(int j = 0 ; j<10; j++)
			{
				if(x == getComponentZOrder(b[i][j]) )
				{
					// Remove the old component
				window.remove(b[i][j]);
					// Replace it with a new component at the same position 
				window.add(new JLabel("Wall"), index);
				return true;
				}
			}
		}
		return false;
	}
	public class SuggestClickListener implements ActionListener 
	{
		public SuggestClickListener(){

		}

		public void actionPerformed(ActionEvent e) {
			Object o =  e.getSource();
			for(int i = 0; i<10 ; i++)
			{
				for(int j = 0 ; j<10; j++)
				{
					if(b[i][j] == o)
					{
						// Find the position of the component within the container
						index = getComponentZOrder(b[i][j]);
						// Remove the old component
						window.remove(b[i][j]);
						// Replace it with a new component at the same position 
						window.add(new JButton("Wall"), index);
					}
				}
				window.repaint();
			}

//			window.validate();
//			window.repaint();
		}
	}

}
