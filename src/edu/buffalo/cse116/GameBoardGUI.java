package edu.buffalo.cse116;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.awt.*;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import edu.buffalo.cse116.Cards;
import java.awt.*;

public class GameBoardGUI extends javax.swing.JFrame 
{
	
	Graphics g;
	private static final long serialVersionUID = 1L;
	private JPanel window;
	private JPanel top;
	private JPanel mid;
	private JPanel bot;
	private JButton card;
	private JPanel botleft;
	private JPanel botmid;
	private JPanel botright;
	private Cards cards;
	private JButton sug;
	  private javax.swing.JButton jButton10;
	  private javax.swing.JButton jButton100;
	  private javax.swing.JButton jButton101;
	  private javax.swing.JButton jButton102;
	  private javax.swing.JButton jButton103;
	  private javax.swing.JButton jButton104;
	  private javax.swing.JButton jButton105;
	  private javax.swing.JButton jButton106;
	  private javax.swing.JButton jButton107;
	  private javax.swing.JButton jButton108;
	  private javax.swing.JButton jButton109;
	  private javax.swing.JButton jButton11;
	  private javax.swing.JButton jButton110;
	  private javax.swing.JButton jButton111;
	  private javax.swing.JButton jButton112;
	  private javax.swing.JButton jButton113;
	  private javax.swing.JButton jButton114;
	  private javax.swing.JButton jButton115;
	  private javax.swing.JButton jButton116;
	  private javax.swing.JButton jButton117;
	  private javax.swing.JButton jButton118;
	  private javax.swing.JButton jButton119;
	  private javax.swing.JButton jButton12;
	  private javax.swing.JButton jButton120;
	  private javax.swing.JButton jButton122;
	  private javax.swing.JButton jButton123;
	  private javax.swing.JButton jButton124;
	  private javax.swing.JButton jButton125;
	  private javax.swing.JButton jButton126;
	  private javax.swing.JButton jButton127;
	  private javax.swing.JButton jButton128;
	  private javax.swing.JButton jButton129;
	  private javax.swing.JButton jButton13;
	  private javax.swing.JButton jButton130;
	  private javax.swing.JButton jButton131;
	  private javax.swing.JButton jButton132;
	  private javax.swing.JButton jButton133;
	  private javax.swing.JButton jButton134;
	  private javax.swing.JButton jButton135;
	  private javax.swing.JButton jButton136;
	  private javax.swing.JButton jButton137;
	  private javax.swing.JButton jButton138;
	  private javax.swing.JButton jButton139;
	  private javax.swing.JButton jButton14;
	  private javax.swing.JButton jButton140;
	  private javax.swing.JButton jButton141;
	  private javax.swing.JButton jButton142;
	  private javax.swing.JButton jButton143;
	  private javax.swing.JButton jButton144;
	  private javax.swing.JButton jButton145;
	  private javax.swing.JButton jButton146;
	  private javax.swing.JButton jButton147;
	  private javax.swing.JButton jButton148;
	  private javax.swing.JButton jButton149;
	  private javax.swing.JButton jButton15;
	  private javax.swing.JButton jButton151;
	  private javax.swing.JButton jButton152;
	  private javax.swing.JButton jButton153;
	  private javax.swing.JButton jButton154;
	  private javax.swing.JButton jButton155;
	  private javax.swing.JButton jButton156;
	  private javax.swing.JButton jButton157;
	  private javax.swing.JButton jButton158;
	  private javax.swing.JButton jButton159;
	  private javax.swing.JButton jButton16;
	  private javax.swing.JButton jButton160;
	  private javax.swing.JButton jButton161;
	  private javax.swing.JButton jButton163;
	  private javax.swing.JButton jButton164;
	  private javax.swing.JButton jButton165;
	  private javax.swing.JButton jButton166;
	  private javax.swing.JButton jButton167;
	  private javax.swing.JButton jButton168;
	  private javax.swing.JButton jButton169;
	  private javax.swing.JButton jButton17;
	  private javax.swing.JButton jButton170;
	  private javax.swing.JButton jButton171;
	  private javax.swing.JButton jButton172;
	  private javax.swing.JButton jButton173;
	  private javax.swing.JButton jButton174;
	  private javax.swing.JButton jButton175;
	  private javax.swing.JButton jButton176;
	  private javax.swing.JButton jButton177;
	  private javax.swing.JButton jButton178;
	  private javax.swing.JButton jButton179;
	  private javax.swing.JButton jButton18;
	  private javax.swing.JButton jButton180;
	  private javax.swing.JButton jButton181;
	  private javax.swing.JButton jButton182;
	  private javax.swing.JButton jButton183;
	  private javax.swing.JButton jButton184;
	  private javax.swing.JButton jButton185;
	  private javax.swing.JButton jButton186;
	  private javax.swing.JButton jButton187;
	  private javax.swing.JButton jButton188;
	  private javax.swing.JButton jButton189;
	  private javax.swing.JButton jButton19;
	  private javax.swing.JButton jButton190;
	  private javax.swing.JButton jButton191;
	  private javax.swing.JButton jButton192;
	  private javax.swing.JButton jButton193;
	  private javax.swing.JButton jButton194;
	  private javax.swing.JButton jButton195;
	  private javax.swing.JButton jButton196;
	  private javax.swing.JButton jButton197;
	  private javax.swing.JButton jButton198;
	  private javax.swing.JButton jButton199;
	  private javax.swing.JButton jButton20;
	  private javax.swing.JButton jButton200;
	  private javax.swing.JButton jButton201;
	  private javax.swing.JButton jButton202;
	  private javax.swing.JButton jButton203;
	  private javax.swing.JButton jButton204;
	  private javax.swing.JButton jButton205;
	  private javax.swing.JButton jButton206;
	  private javax.swing.JButton jButton207;
	  private javax.swing.JButton jButton208;
	  private javax.swing.JButton jButton209;
	  private javax.swing.JButton jButton21;
	  private javax.swing.JButton jButton210;
	  private javax.swing.JButton jButton211;
	  private javax.swing.JButton jButton212;
	  private javax.swing.JButton jButton213;
	  private javax.swing.JButton jButton214;
	  private javax.swing.JButton jButton215;
	  private javax.swing.JButton jButton216;
	  private javax.swing.JButton jButton217;
	  private javax.swing.JButton jButton218;
	  private javax.swing.JButton jButton219;
	  private javax.swing.JButton jButton22;
	  private javax.swing.JButton jButton220;
	  private javax.swing.JButton jButton221;
	  private javax.swing.JButton jButton222;
	  private javax.swing.JButton jButton223;
	  private javax.swing.JButton jButton224;
	  private javax.swing.JButton jButton225;
	  private javax.swing.JButton jButton226;
	  private javax.swing.JButton jButton228;
	  private javax.swing.JButton jButton229;
	  private javax.swing.JButton jButton23;
	  private javax.swing.JButton jButton230;
	  private javax.swing.JButton jButton231;
	  private javax.swing.JButton jButton232;
	  private javax.swing.JButton jButton233;
	  private javax.swing.JButton jButton234;
	  private javax.swing.JButton jButton235;
	  private javax.swing.JButton jButton236;
	  private javax.swing.JButton jButton237;
	  private javax.swing.JButton jButton238;
	  private javax.swing.JButton jButton239;
	  private javax.swing.JButton jButton24;
	  private javax.swing.JButton jButton240;
	  private javax.swing.JButton jButton241;
	  private javax.swing.JButton jButton242;
	  private javax.swing.JButton jButton243;
	  private javax.swing.JButton jButton244;
	  private javax.swing.JButton jButton245;
	  private javax.swing.JButton jButton246;
	  private javax.swing.JButton jButton247;
	  private javax.swing.JButton jButton248;
	  private javax.swing.JButton jButton249;
	  private javax.swing.JButton jButton250;
	  private javax.swing.JButton jButton251;
	  private javax.swing.JButton jButton252;
	  private javax.swing.JButton jButton253;
	  private javax.swing.JButton jButton254;
	  private javax.swing.JButton jButton255;
	  private javax.swing.JButton jButton256;
	  private javax.swing.JButton jButton257;
	  private javax.swing.JButton jButton258;
	  private javax.swing.JButton jButton259;
	  private javax.swing.JButton jButton26;
	  private javax.swing.JButton jButton260;
	  private javax.swing.JButton jButton261;
	  private javax.swing.JButton jButton262;
	  private javax.swing.JButton jButton263;
	  private javax.swing.JButton jButton264;
	  private javax.swing.JButton jButton265;
	  private javax.swing.JButton jButton266;
	  private javax.swing.JButton jButton267;
	  private javax.swing.JButton jButton268;
	  private javax.swing.JButton jButton269;
	  private javax.swing.JButton jButton270;
	  private javax.swing.JButton jButton271;
	  private javax.swing.JButton jButton272;
	  private javax.swing.JButton jButton273;
	  private javax.swing.JButton jButton274;
	  private javax.swing.JButton jButton275;
	  private javax.swing.JButton jButton276;
	  private javax.swing.JButton jButton277;
	  private javax.swing.JButton jButton278;
	  private javax.swing.JButton jButton279;
	  private javax.swing.JButton jButton28;
	  private javax.swing.JButton jButton280;
	  private javax.swing.JButton jButton281;
	  private javax.swing.JButton jButton282;
	  private javax.swing.JButton jButton284;
	  private javax.swing.JButton jButton285;
	  private javax.swing.JButton jButton286;
	  private javax.swing.JButton jButton287;
	  private javax.swing.JButton jButton288;
	  private javax.swing.JButton jButton289;
	  private javax.swing.JButton jButton29;
	  private javax.swing.JButton jButton290;
	  private javax.swing.JButton jButton291;
	  private javax.swing.JButton jButton292;
	  private javax.swing.JButton jButton293;
	  private javax.swing.JButton jButton294;
	  private javax.swing.JButton jButton295;
	  private javax.swing.JButton jButton296;
	  private javax.swing.JButton jButton298;
	  private javax.swing.JButton jButton299;
	  private javax.swing.JButton jButton30;
	  private javax.swing.JButton jButton300;
	  private javax.swing.JButton jButton301;
	  private javax.swing.JButton jButton302;
	  private javax.swing.JButton jButton303;
	  private javax.swing.JButton jButton304;
	  private javax.swing.JButton jButton305;
	  private javax.swing.JButton jButton307;
	  private javax.swing.JButton jButton308;
	  private javax.swing.JButton jButton309;
	  private javax.swing.JButton jButton31;
	  private javax.swing.JButton jButton311;
	  private javax.swing.JButton jButton312;
	  private javax.swing.JButton jButton313;
	  private javax.swing.JButton jButton314;
	  private javax.swing.JButton jButton315;
	  private javax.swing.JButton jButton316;
	  private javax.swing.JButton jButton317;
	  private javax.swing.JButton jButton318;
	  private javax.swing.JButton jButton32;
	  private javax.swing.JButton jButton320;
	  private javax.swing.JButton jButton321;
	  private javax.swing.JButton jButton323;
	  private javax.swing.JButton jButton324;
	  private javax.swing.JButton jButton326;
	  private javax.swing.JButton jButton327;
	  private javax.swing.JButton jButton328;
	  private javax.swing.JButton jButton33;
	  private javax.swing.JButton jButton330;
	  private javax.swing.JButton jButton331;
	  private javax.swing.JButton jButton332;
	  private javax.swing.JButton jButton333;
	  private javax.swing.JButton jButton334;
	  private javax.swing.JButton jButton335;
	  private javax.swing.JButton jButton337;
	  private javax.swing.JButton jButton338;
	  private javax.swing.JButton jButton339;
	  private javax.swing.JButton jButton340;
	  private javax.swing.JButton jButton341;
	  private javax.swing.JButton jButton343;
	  private javax.swing.JButton jButton344;
	  private javax.swing.JButton jButton347;
	  private javax.swing.JButton jButton348;
	  private javax.swing.JButton jButton35;
	  private javax.swing.JButton jButton350;
	  private javax.swing.JButton jButton351;
	  private javax.swing.JButton jButton352;
	  private javax.swing.JButton jButton353;
	  private javax.swing.JButton jButton354;
	  private javax.swing.JButton jButton357;
	  private javax.swing.JButton jButton358;
	  private javax.swing.JButton jButton359;
	  private javax.swing.JButton jButton36;
	  private javax.swing.JButton jButton360;
	  private javax.swing.JButton jButton362;
	  private javax.swing.JButton jButton363;
	  private javax.swing.JButton jButton364;
	  private javax.swing.JButton jButton365;
	  private javax.swing.JButton jButton366;
	  private javax.swing.JButton jButton367;
	  private javax.swing.JButton jButton369;
	  private javax.swing.JButton jButton37;
	  private javax.swing.JButton jButton370;
	  private javax.swing.JButton jButton372;
	  private javax.swing.JButton jButton373;
	  private javax.swing.JButton jButton375;
	  private javax.swing.JButton jButton376;
	  private javax.swing.JButton jButton377;
	  private javax.swing.JButton jButton379;
	  private javax.swing.JButton jButton38;
	  private javax.swing.JButton jButton380;
	  private javax.swing.JButton jButton382;
	  private javax.swing.JButton jButton383;
	  private javax.swing.JButton jButton385;
	  private javax.swing.JButton jButton386;
	  private javax.swing.JButton jButton387;
	  private javax.swing.JButton jButton389;
	  private javax.swing.JButton jButton39;
	  private javax.swing.JButton jButton390;
	  private javax.swing.JButton jButton391;
	  private javax.swing.JButton jButton392;
	  private javax.swing.JButton jButton393;
	  private javax.swing.JButton jButton394;
	  private javax.swing.JButton jButton395;
	  private javax.swing.JButton jButton396;
	  private javax.swing.JButton jButton397;
	  private javax.swing.JButton jButton398;
	  private javax.swing.JButton jButton399;
	  private javax.swing.JButton jButton40;
	  private javax.swing.JButton jButton400;
	  private javax.swing.JButton jButton401;
	  private javax.swing.JButton jButton402;
	  private javax.swing.JButton jButton403;
	  private javax.swing.JButton jButton404;
	  private javax.swing.JButton jButton405;
	  private javax.swing.JButton jButton406;
	  private javax.swing.JButton jButton407;
	  private javax.swing.JButton jButton408;
	  private javax.swing.JButton jButton409;
	  private javax.swing.JButton jButton41;
	  private javax.swing.JButton jButton410;
	  private javax.swing.JButton jButton411;
	  private javax.swing.JButton jButton412;
	  private javax.swing.JButton jButton413;
	  private javax.swing.JButton jButton414;
	  private javax.swing.JButton jButton42;
	  private javax.swing.JButton jButton43;
	  private javax.swing.JButton jButton44;
	  private javax.swing.JButton jButton47;
	  private javax.swing.JButton jButton48;
	  private javax.swing.JButton jButton49;
	  private javax.swing.JButton jButton50;
	  private javax.swing.JButton jButton51;
	  private javax.swing.JButton jButton53;
	  private javax.swing.JButton jButton54;
	  private javax.swing.JButton jButton55;
	  private javax.swing.JButton jButton56;
	  private javax.swing.JButton jButton57;
	  private javax.swing.JButton jButton58;
	  private javax.swing.JButton jButton59;
	  private javax.swing.JButton jButton62;
	  private javax.swing.JButton jButton63;
	  private javax.swing.JButton jButton64;
	  private javax.swing.JButton jButton65;
	  private javax.swing.JButton jButton66;
	  
	  private javax.swing.JButton jButton68;
	  private javax.swing.JButton jButton69;
	  private javax.swing.JButton jButton70;
	  private javax.swing.JButton jButton71;
	  private javax.swing.JButton jButton72;
	  private javax.swing.JButton jButton73;
	  private javax.swing.JButton jButton74;
	  private javax.swing.JButton jButton75;
	  private javax.swing.JButton jButton76;
	  private javax.swing.JButton jButton77;
	  private javax.swing.JButton jButton78;
	  private javax.swing.JButton jButton8;
	  private javax.swing.JButton jButton80;
	  private javax.swing.JButton jButton81;
	  private javax.swing.JButton jButton82;
	  private javax.swing.JButton jButton83;
	  private javax.swing.JButton jButton84;
	  private javax.swing.JButton jButton85;
	  private javax.swing.JButton jButton86;
	  private javax.swing.JButton jButton88;
	  private javax.swing.JButton jButton89;
	  private javax.swing.JButton jButton9;
	  private javax.swing.JButton jButton90;
	  private javax.swing.JButton jButton91;
	  private javax.swing.JButton jButton92;
	  private javax.swing.JButton jButton94;
	  private javax.swing.JButton jButton95;
	  private javax.swing.JButton jButton96;
	  private javax.swing.JButton jButton97;
	  private javax.swing.JButton jButton98;
	  private javax.swing.JButton jButton99;
	  private javax.swing.JLabel jLabel1;
	  private javax.swing.JLabel jLabel10;
	  private javax.swing.JLabel jLabel2;
	  private javax.swing.JLabel jLabel3;
	  private javax.swing.JLabel jLabel4;
	  private javax.swing.JLabel jLabel5;
	  private javax.swing.JLabel jLabel6;
	  private javax.swing.JLabel jLabel7;
	  private javax.swing.JLabel jLabel8;
	  private javax.swing.JLabel jLabel9;



	
	public GameBoardGUI(Player player) throws IOException
	{
	  setLocation(600,25);
	  window = new javax.swing.JPanel(new BorderLayout());
	  top = new javax.swing.JPanel();
      mid = new javax.swing.JPanel();
      bot = new javax.swing.JPanel();
      botleft = new JPanel(new BorderLayout());
      botmid = new JPanel();
      botright = new JPanel();
      top.setOpaque(true);
      top.setBackground(Color.GREEN);
      
      bot.add(botleft);
      bot.add(botmid);
      bot.add(botright);
      cardButtonMaker(player);
//      userPortrait();
      
      top.setLayout(new GridLayout(27,24));
      window.add(top, BorderLayout.PAGE_START);
      window.add(mid);
      window.add(bot, BorderLayout.PAGE_END);
      this.add(window);
      
	setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	  mid.setSize(800,25);
	  top.setBorder(javax.swing.BorderFactory.createTitledBorder("Cluedo Board"));
      mid.setBorder(javax.swing.BorderFactory.createTitledBorder(player.returnCharacter()));
      botmid.setBorder(BorderFactory.createTitledBorder("Cards in Your Hand"));
      
      java.awt.GridBagConstraints gridBagConstraints;

      jButton8 = new javax.swing.JButton();
      jButton9 = new javax.swing.JButton();
      jButton10 = new javax.swing.JButton();
      jButton11 = new javax.swing.JButton();
      jButton12 = new javax.swing.JButton();
      jButton13 = new javax.swing.JButton();
      jButton14 = new javax.swing.JButton();
      jButton15 = new javax.swing.JButton();
      jButton16 = new javax.swing.JButton();
      jButton17 = new javax.swing.JButton();
      jButton18 = new javax.swing.JButton();
      jButton19 = new javax.swing.JButton();
      jButton20 = new javax.swing.JButton();
      jButton21 = new javax.swing.JButton();
      jButton22 = new javax.swing.JButton();
      jButton23 = new javax.swing.JButton();
      jButton24 = new javax.swing.JButton();
      jButton26 = new javax.swing.JButton();
      jButton28 = new javax.swing.JButton();
      jButton29 = new javax.swing.JButton();
      jButton30 = new javax.swing.JButton();
      jButton31 = new javax.swing.JButton();
      jButton32 = new javax.swing.JButton();
      jButton33 = new javax.swing.JButton();
      jButton35 = new javax.swing.JButton();
      jButton36 = new javax.swing.JButton();
      jButton37 = new javax.swing.JButton();
      jButton38 = new javax.swing.JButton();
      jButton39 = new javax.swing.JButton();
      jButton40 = new javax.swing.JButton();
      jButton41 = new javax.swing.JButton();
      jButton42 = new javax.swing.JButton();
      jButton43 = new javax.swing.JButton();
      jButton44 = new javax.swing.JButton();
      jButton47 = new javax.swing.JButton();
      jButton48 = new javax.swing.JButton();
      jButton49 = new javax.swing.JButton();
      jButton50 = new javax.swing.JButton();
      jButton51 = new javax.swing.JButton();
      jButton53 = new javax.swing.JButton();
      jButton54 = new javax.swing.JButton();
      jButton55 = new javax.swing.JButton();
      jButton56 = new javax.swing.JButton();
      jButton57 = new javax.swing.JButton();
      jButton58 = new javax.swing.JButton();
      jButton59 = new javax.swing.JButton();
      jButton62 = new javax.swing.JButton();
      jButton63 = new javax.swing.JButton();
      jButton64 = new javax.swing.JButton();
      jButton65 = new javax.swing.JButton();
      jButton66 = new javax.swing.JButton();
      
      jButton68 = new javax.swing.JButton();
      jButton69 = new javax.swing.JButton();
      jButton70 = new javax.swing.JButton();
      jButton71 = new javax.swing.JButton();
      jButton72 = new javax.swing.JButton();
      jButton73 = new javax.swing.JButton();
      jButton74 = new javax.swing.JButton();
      jButton75 = new javax.swing.JButton();
      jButton76 = new javax.swing.JButton();
      jButton77 = new javax.swing.JButton();
      jButton78 = new javax.swing.JButton();
      jButton80 = new javax.swing.JButton();
      jButton81 = new javax.swing.JButton();
      jButton82 = new javax.swing.JButton();
      jButton83 = new javax.swing.JButton();
      jButton84 = new javax.swing.JButton();
      jButton85 = new javax.swing.JButton();
      jButton86 = new javax.swing.JButton();
      jButton88 = new javax.swing.JButton();
      jButton89 = new javax.swing.JButton();
      jButton90 = new javax.swing.JButton();
      jButton91 = new javax.swing.JButton();
      jButton92 = new javax.swing.JButton();
      jButton94 = new javax.swing.JButton();
      jButton95 = new javax.swing.JButton();
      jButton96 = new javax.swing.JButton();
      jButton97 = new javax.swing.JButton();
      jButton98 = new javax.swing.JButton();
      jButton99 = new javax.swing.JButton();
      jButton100 = new javax.swing.JButton();
      jButton101 = new javax.swing.JButton();
      jButton102 = new javax.swing.JButton();
      jButton103 = new javax.swing.JButton();
      jButton104 = new javax.swing.JButton();
      jButton105 = new javax.swing.JButton();
      jButton106 = new javax.swing.JButton();
      jButton107 = new javax.swing.JButton();
      jButton108 = new javax.swing.JButton();
      jButton109 = new javax.swing.JButton();
      jButton110 = new javax.swing.JButton();
      jButton111 = new javax.swing.JButton();
      jButton112 = new javax.swing.JButton();
      jButton113 = new javax.swing.JButton();
      jButton114 = new javax.swing.JButton();
      jButton115 = new javax.swing.JButton();
      jButton116 = new javax.swing.JButton();
      jButton117 = new javax.swing.JButton();
      jButton118 = new javax.swing.JButton();
      jButton119 = new javax.swing.JButton();
      jButton120 = new javax.swing.JButton();
      jButton122 = new javax.swing.JButton();
      jButton123 = new javax.swing.JButton();
      jButton124 = new javax.swing.JButton();
      jButton125 = new javax.swing.JButton();
      jButton126 = new javax.swing.JButton();
      jButton127 = new javax.swing.JButton();
      jButton128 = new javax.swing.JButton();
      jButton129 = new javax.swing.JButton();
      jButton130 = new javax.swing.JButton();
      jButton131 = new javax.swing.JButton();
      jButton132 = new javax.swing.JButton();
      jButton133 = new javax.swing.JButton();
      jButton134 = new javax.swing.JButton();
      jButton135 = new javax.swing.JButton();
      jButton136 = new javax.swing.JButton();
      jButton137 = new javax.swing.JButton();
      jButton138 = new javax.swing.JButton();
      jButton139 = new javax.swing.JButton();
      jButton140 = new javax.swing.JButton();
      jButton141 = new javax.swing.JButton();
      jButton142 = new javax.swing.JButton();
      jButton143 = new javax.swing.JButton();
      jButton144 = new javax.swing.JButton();
      jButton145 = new javax.swing.JButton();
      jButton146 = new javax.swing.JButton();
      jButton147 = new javax.swing.JButton();
      jButton148 = new javax.swing.JButton();
      jButton149 = new javax.swing.JButton();
      jButton151 = new javax.swing.JButton();
      jButton152 = new javax.swing.JButton();
      jButton153 = new javax.swing.JButton();
      jButton154 = new javax.swing.JButton();
      jButton155 = new javax.swing.JButton();
      jButton156 = new javax.swing.JButton();
      jButton157 = new javax.swing.JButton();
      jButton158 = new javax.swing.JButton();
      jButton159 = new javax.swing.JButton();
      jButton160 = new javax.swing.JButton();
      jButton161 = new javax.swing.JButton();
      jButton163 = new javax.swing.JButton();
      jButton164 = new javax.swing.JButton();
      jButton165 = new javax.swing.JButton();
      jButton166 = new javax.swing.JButton();
      jButton167 = new javax.swing.JButton();
      jButton168 = new javax.swing.JButton();
      jButton169 = new javax.swing.JButton();
      jButton170 = new javax.swing.JButton();
      jButton171 = new javax.swing.JButton();
      jButton172 = new javax.swing.JButton();
      jButton173 = new javax.swing.JButton();
      jButton174 = new javax.swing.JButton();
      jButton175 = new javax.swing.JButton();
      jButton176 = new javax.swing.JButton();
      jButton177 = new javax.swing.JButton();
      jButton178 = new javax.swing.JButton();
      jButton179 = new javax.swing.JButton();
      jButton180 = new javax.swing.JButton();
      jButton181 = new javax.swing.JButton();
      jButton182 = new javax.swing.JButton();
      jButton183 = new javax.swing.JButton();
      jButton184 = new javax.swing.JButton();
      jButton185 = new javax.swing.JButton();
      jButton186 = new javax.swing.JButton();
      jButton187 = new javax.swing.JButton();
      jButton188 = new javax.swing.JButton();
      jButton189 = new javax.swing.JButton();
      jButton190 = new javax.swing.JButton();
      jButton191 = new javax.swing.JButton();
      jButton192 = new javax.swing.JButton();
      jButton193 = new javax.swing.JButton();
      jButton194 = new javax.swing.JButton();
      jButton195 = new javax.swing.JButton();
      jButton196 = new javax.swing.JButton();
      jButton197 = new javax.swing.JButton();
      jButton198 = new javax.swing.JButton();
      jButton199 = new javax.swing.JButton();
      jButton200 = new javax.swing.JButton();
      jButton201 = new javax.swing.JButton();
      jButton202 = new javax.swing.JButton();
      jButton203 = new javax.swing.JButton();
      jButton204 = new javax.swing.JButton();
      jButton205 = new javax.swing.JButton();
      jButton206 = new javax.swing.JButton();
      jButton207 = new javax.swing.JButton();
      jButton208 = new javax.swing.JButton();
      jButton209 = new javax.swing.JButton();
      jButton210 = new javax.swing.JButton();
      jButton211 = new javax.swing.JButton();
      jButton212 = new javax.swing.JButton();
      jButton213 = new javax.swing.JButton();
      jButton214 = new javax.swing.JButton();
      jButton215 = new javax.swing.JButton();
      jButton216 = new javax.swing.JButton();
      jButton217 = new javax.swing.JButton();
      jButton218 = new javax.swing.JButton();
      jButton219 = new javax.swing.JButton();
      jButton220 = new javax.swing.JButton();
      jButton221 = new javax.swing.JButton();
      jButton222 = new javax.swing.JButton();
      jButton223 = new javax.swing.JButton();
      jButton224 = new javax.swing.JButton();
      jButton225 = new javax.swing.JButton();
      jButton226 = new javax.swing.JButton();
      jButton228 = new javax.swing.JButton();
      jButton229 = new javax.swing.JButton();
      jButton230 = new javax.swing.JButton();
      jButton231 = new javax.swing.JButton();
      jButton232 = new javax.swing.JButton();
      jButton233 = new javax.swing.JButton();
      jButton234 = new javax.swing.JButton();
      jButton235 = new javax.swing.JButton();
      jButton236 = new javax.swing.JButton();
      jButton237 = new javax.swing.JButton();
      jButton238 = new javax.swing.JButton();
      jButton239 = new javax.swing.JButton();
      jButton240 = new javax.swing.JButton();
      jButton241 = new javax.swing.JButton();
      jButton242 = new javax.swing.JButton();
      jButton243 = new javax.swing.JButton();
      jButton244 = new javax.swing.JButton();
      jButton245 = new javax.swing.JButton();
      jButton246 = new javax.swing.JButton();
      jButton247 = new javax.swing.JButton();
      jButton248 = new javax.swing.JButton();
      jButton249 = new javax.swing.JButton();
      jButton250 = new javax.swing.JButton();
      jButton251 = new javax.swing.JButton();
      jButton252 = new javax.swing.JButton();
      jButton253 = new javax.swing.JButton();
      jButton254 = new javax.swing.JButton();
      jButton255 = new javax.swing.JButton();
      jButton256 = new javax.swing.JButton();
      jButton257 = new javax.swing.JButton();
      jButton258 = new javax.swing.JButton();
      jButton259 = new javax.swing.JButton();
      jButton260 = new javax.swing.JButton();
      jButton261 = new javax.swing.JButton();
      jButton262 = new javax.swing.JButton();
      jButton263 = new javax.swing.JButton();
      jButton264 = new javax.swing.JButton();
      jButton265 = new javax.swing.JButton();
      jButton266 = new javax.swing.JButton();
      jButton267 = new javax.swing.JButton();
      jButton268 = new javax.swing.JButton();
      jButton269 = new javax.swing.JButton();
      jButton270 = new javax.swing.JButton();
      jButton271 = new javax.swing.JButton();
      jButton272 = new javax.swing.JButton();
      jButton273 = new javax.swing.JButton();
      jButton274 = new javax.swing.JButton();
      jButton275 = new javax.swing.JButton();
      jButton276 = new javax.swing.JButton();
      jButton277 = new javax.swing.JButton();
      jButton278 = new javax.swing.JButton();
      jButton279 = new javax.swing.JButton();
      jButton280 = new javax.swing.JButton();
      jButton281 = new javax.swing.JButton();
      jButton282 = new javax.swing.JButton();
      jButton284 = new javax.swing.JButton();
      jButton285 = new javax.swing.JButton();
      jButton286 = new javax.swing.JButton();
      jButton287 = new javax.swing.JButton();
      jButton288 = new javax.swing.JButton();
      jButton289 = new javax.swing.JButton();
      jButton290 = new javax.swing.JButton();
      jButton291 = new javax.swing.JButton();
      jButton292 = new javax.swing.JButton();
      jButton293 = new javax.swing.JButton();
      jButton294 = new javax.swing.JButton();
      jButton295 = new javax.swing.JButton();
      jButton296 = new javax.swing.JButton();
      jButton298 = new javax.swing.JButton();
      jButton299 = new javax.swing.JButton();
      jButton300 = new javax.swing.JButton();
      jButton301 = new javax.swing.JButton();
      jButton302 = new javax.swing.JButton();
      jButton303 = new javax.swing.JButton();
      jButton304 = new javax.swing.JButton();
      jButton305 = new javax.swing.JButton();
      jButton307 = new javax.swing.JButton();
      jButton308 = new javax.swing.JButton();
      jButton309 = new javax.swing.JButton();
      jButton311 = new javax.swing.JButton();
      jButton312 = new javax.swing.JButton();
      jButton313 = new javax.swing.JButton();
      jButton314 = new javax.swing.JButton();
      jButton315 = new javax.swing.JButton();
      jButton316 = new javax.swing.JButton();
      jButton317 = new javax.swing.JButton();
      jButton318 = new javax.swing.JButton();
      jButton320 = new javax.swing.JButton();
      jButton321 = new javax.swing.JButton();
      jButton323 = new javax.swing.JButton();
      jButton324 = new javax.swing.JButton();
      jButton326 = new javax.swing.JButton();
      jButton327 = new javax.swing.JButton();
      jButton328 = new javax.swing.JButton();
      jButton330 = new javax.swing.JButton();
      jButton331 = new javax.swing.JButton();
      jButton332 = new javax.swing.JButton();
      jButton333 = new javax.swing.JButton();
      jButton334 = new javax.swing.JButton();
      jButton335 = new javax.swing.JButton();
      jButton337 = new javax.swing.JButton();
      jButton338 = new javax.swing.JButton();
      jButton339 = new javax.swing.JButton();
      jButton340 = new javax.swing.JButton();
      jButton341 = new javax.swing.JButton();
      jButton343 = new javax.swing.JButton();
      jButton344 = new javax.swing.JButton();
      jButton347 = new javax.swing.JButton();
      jButton348 = new javax.swing.JButton();
      jButton350 = new javax.swing.JButton();
      jButton351 = new javax.swing.JButton();
      jButton352 = new javax.swing.JButton();
      jButton353 = new javax.swing.JButton();
      jButton354 = new javax.swing.JButton();
      jButton357 = new javax.swing.JButton();
      jButton358 = new javax.swing.JButton();
      jButton359 = new javax.swing.JButton();
      jButton360 = new javax.swing.JButton();
      jButton362 = new javax.swing.JButton();
      jButton363 = new javax.swing.JButton();
      jButton364 = new javax.swing.JButton();
      jButton365 = new javax.swing.JButton();
      jButton366 = new javax.swing.JButton();
      jButton367 = new javax.swing.JButton();
      jButton369 = new javax.swing.JButton();
      jButton370 = new javax.swing.JButton();
      jButton372 = new javax.swing.JButton();
      jButton373 = new javax.swing.JButton();
      jButton375 = new javax.swing.JButton();
      jButton376 = new javax.swing.JButton();
      jButton377 = new javax.swing.JButton();
      jButton379 = new javax.swing.JButton();
      jButton380 = new javax.swing.JButton();
      jButton382 = new javax.swing.JButton();
      jButton383 = new javax.swing.JButton();
      jButton385 = new javax.swing.JButton();
      jButton386 = new javax.swing.JButton();
      jButton387 = new javax.swing.JButton();
      jButton389 = new javax.swing.JButton();
      jButton390 = new javax.swing.JButton();
      jButton391 = new javax.swing.JButton();
      jButton392 = new javax.swing.JButton();
      jButton393 = new javax.swing.JButton();
      jButton394 = new javax.swing.JButton();
      jButton395 = new javax.swing.JButton();
      jButton396 = new javax.swing.JButton();
      jButton397 = new javax.swing.JButton();
      jButton398 = new javax.swing.JButton();
      jButton399 = new javax.swing.JButton();
      jButton400 = new javax.swing.JButton();
      jButton401 = new javax.swing.JButton();
      jButton402 = new javax.swing.JButton();
      jButton403 = new javax.swing.JButton();
      jButton404 = new javax.swing.JButton();
      jButton405 = new javax.swing.JButton();
      jButton406 = new javax.swing.JButton();
      jButton407 = new javax.swing.JButton();
      jButton408 = new javax.swing.JButton();
      jButton409 = new javax.swing.JButton();
      jButton410 = new javax.swing.JButton();
      jButton411 = new javax.swing.JButton();
      jButton412 = new javax.swing.JButton();
      jButton413 = new javax.swing.JButton();
      jButton414 = new javax.swing.JButton();
      jLabel2 = new javax.swing.JLabel();
      jLabel3 = new javax.swing.JLabel();
      jLabel4 = new javax.swing.JLabel();
      jLabel5 = new javax.swing.JLabel();
      jLabel6 = new javax.swing.JLabel();
      jLabel7 = new javax.swing.JLabel();
      jLabel8 = new javax.swing.JLabel();
      jLabel9 = new javax.swing.JLabel();
      jLabel10 = new javax.swing.JLabel();
      jLabel1 = new javax.swing.JLabel();

      setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

      top.setBorder(javax.swing.BorderFactory.createTitledBorder("Cluedo Board"));
      top.setLayout(new java.awt.GridBagLayout());

      jButton8.setMargin(new Insets(0,0,0,0));
      jButton9.setMargin(new Insets(0,0,0,0));
      jButton10.setMargin(new Insets(0,0,0,0));
      jButton11.setMargin(new Insets(0,0,0,0));
      jButton12.setMargin(new Insets(0,0,0,0));
      jButton13.setMargin(new Insets(0,0,0,0));
      jButton14.setMargin(new Insets(0,0,0,0));
      jButton15.setMargin(new Insets(0,0,0,0));
      jButton16.setMargin(new Insets(0,0,0,0));
      jButton17.setMargin(new Insets(0,0,0,0));
      jButton18.setMargin(new Insets(0,0,0,0));
      jButton19.setMargin(new Insets(0,0,0,0));
      jButton20.setMargin(new Insets(0,0,0,0));
      jButton21.setMargin(new Insets(0,0,0,0));
      jButton22.setMargin(new Insets(0,0,0,0));
      jButton23.setMargin(new Insets(0,0,0,0));
      jButton24.setMargin(new Insets(0,0,0,0));
      jButton26.setMargin(new Insets(0,0,0,0));
      jButton28.setMargin(new Insets(0,0,0,0));
      jButton29.setMargin(new Insets(0,0,0,0));
      jButton30.setMargin(new Insets(0,0,0,0));
      jButton31.setMargin(new Insets(0,0,0,0));
      jButton32.setMargin(new Insets(0,0,0,0));
      jButton33.setMargin(new Insets(0,0,0,0));
      jButton35.setMargin(new Insets(0,0,0,0));
      jButton36.setMargin(new Insets(0,0,0,0));
      jButton37.setMargin(new Insets(0,0,0,0));
      jButton38.setMargin(new Insets(0,0,0,0));
      jButton39.setMargin(new Insets(0,0,0,0));
      jButton40.setMargin(new Insets(0,0,0,0));
      jButton41.setMargin(new Insets(0,0,0,0));
      jButton42.setMargin(new Insets(0,0,0,0));
      jButton43.setMargin(new Insets(0,0,0,0));
      jButton44.setMargin(new Insets(0,0,0,0));
      jButton47.setMargin(new Insets(0,0,0,0));
      jButton48.setMargin(new Insets(0,0,0,0));
      jButton49.setMargin(new Insets(0,0,0,0));
      jButton50.setMargin(new Insets(0,0,0,0));
      jButton51.setMargin(new Insets(0,0,0,0));
      jButton53.setMargin(new Insets(0,0,0,0));
      jButton54.setMargin(new Insets(0,0,0,0));
      jButton55.setMargin(new Insets(0,0,0,0));
      jButton56.setMargin(new Insets(0,0,0,0));
      jButton57.setMargin(new Insets(0,0,0,0));
      jButton58.setMargin(new Insets(0,0,0,0));
      jButton59.setMargin(new Insets(0,0,0,0));
      jButton62.setMargin(new Insets(0,0,0,0));
      jButton63.setMargin(new Insets(0,0,0,0));
      jButton64.setMargin(new Insets(0,0,0,0));
      jButton65.setMargin(new Insets(0,0,0,0));
      jButton66.setMargin(new Insets(0,0,0,0));
      jButton68.setMargin(new Insets(0,0,0,0));
      jButton69.setMargin(new Insets(0,0,0,0));
      jButton70.setMargin(new Insets(0,0,0,0));
      jButton71.setMargin(new Insets(0,0,0,0));
      jButton72.setMargin(new Insets(0,0,0,0));
      jButton73.setMargin(new Insets(0,0,0,0));
      jButton74.setMargin(new Insets(0,0,0,0));
      jButton75.setMargin(new Insets(0,0,0,0));
      jButton76.setMargin(new Insets(0,0,0,0));
      jButton77.setMargin(new Insets(0,0,0,0));
      jButton78.setMargin(new Insets(0,0,0,0));
      jButton80.setMargin(new Insets(0,0,0,0));
      jButton81.setMargin(new Insets(0,0,0,0));
      jButton82.setMargin(new Insets(0,0,0,0));
      jButton83.setMargin(new Insets(0,0,0,0));
      jButton84.setMargin(new Insets(0,0,0,0));
      jButton85.setMargin(new Insets(0,0,0,0));
      jButton86.setMargin(new Insets(0,0,0,0));
      jButton88.setMargin(new Insets(0,0,0,0));
      jButton89.setMargin(new Insets(0,0,0,0));
      jButton90.setMargin(new Insets(0,0,0,0));
      jButton91.setMargin(new Insets(0,0,0,0));
      jButton92.setMargin(new Insets(0,0,0,0));
      jButton94.setMargin(new Insets(0,0,0,0));
      jButton95.setMargin(new Insets(0,0,0,0));
      jButton96.setMargin(new Insets(0,0,0,0));
      jButton97.setMargin(new Insets(0,0,0,0));
      jButton98.setMargin(new Insets(0,0,0,0));
      jButton99.setMargin(new Insets(0,0,0,0));
      jButton100.setMargin(new Insets(0,0,0,0));
      jButton101.setMargin(new Insets(0,0,0,0));
      jButton102.setMargin(new Insets(0,0,0,0));
      jButton103.setMargin(new Insets(0,0,0,0));
      jButton104.setMargin(new Insets(0,0,0,0));
      jButton105.setMargin(new Insets(0,0,0,0));
      jButton106.setMargin(new Insets(0,0,0,0));
      jButton107.setMargin(new Insets(0,0,0,0));
      jButton108.setMargin(new Insets(0,0,0,0));
      jButton109.setMargin(new Insets(0,0,0,0));
      jButton110.setMargin(new Insets(0,0,0,0));
      jButton111.setMargin(new Insets(0,0,0,0));
      jButton112.setMargin(new Insets(0,0,0,0));
      jButton113.setMargin(new Insets(0,0,0,0));
      jButton114.setMargin(new Insets(0,0,0,0));
      jButton115.setMargin(new Insets(0,0,0,0));
      jButton116.setMargin(new Insets(0,0,0,0));
      jButton117.setMargin(new Insets(0,0,0,0));
      jButton118.setMargin(new Insets(0,0,0,0));
      jButton119.setMargin(new Insets(0,0,0,0));
      jButton120.setMargin(new Insets(0,0,0,0));
      jButton122.setMargin(new Insets(0,0,0,0));
      jButton123.setMargin(new Insets(0,0,0,0));
      jButton124.setMargin(new Insets(0,0,0,0));
      jButton125.setMargin(new Insets(0,0,0,0));
      jButton126.setMargin(new Insets(0,0,0,0));
      jButton127.setMargin(new Insets(0,0,0,0));
      jButton128.setMargin(new Insets(0,0,0,0));
      jButton129.setMargin(new Insets(0,0,0,0));
      jButton130.setMargin(new Insets(0,0,0,0));
      jButton131.setMargin(new Insets(0,0,0,0));
      jButton132.setMargin(new Insets(0,0,0,0));
      jButton133.setMargin(new Insets(0,0,0,0));
      jButton134.setMargin(new Insets(0,0,0,0));
      jButton135.setMargin(new Insets(0,0,0,0));
      jButton136.setMargin(new Insets(0,0,0,0));
      jButton137.setMargin(new Insets(0,0,0,0));
      jButton138.setMargin(new Insets(0,0,0,0));
      jButton139.setMargin(new Insets(0,0,0,0));
      jButton140.setMargin(new Insets(0,0,0,0));
      jButton141.setMargin(new Insets(0,0,0,0));
      jButton142.setMargin(new Insets(0,0,0,0));
      jButton143.setMargin(new Insets(0,0,0,0));
      jButton144.setMargin(new Insets(0,0,0,0));
      jButton145.setMargin(new Insets(0,0,0,0));
      jButton146.setMargin(new Insets(0,0,0,0));
      jButton147.setMargin(new Insets(0,0,0,0));
      jButton148.setMargin(new Insets(0,0,0,0));
      jButton149.setMargin(new Insets(0,0,0,0));
      jButton151.setMargin(new Insets(0,0,0,0));
      jButton152.setMargin(new Insets(0,0,0,0));
      jButton153.setMargin(new Insets(0,0,0,0));
      jButton154.setMargin(new Insets(0,0,0,0));
      jButton155.setMargin(new Insets(0,0,0,0));
      jButton156.setMargin(new Insets(0,0,0,0));
      jButton157.setMargin(new Insets(0,0,0,0));
      jButton158.setMargin(new Insets(0,0,0,0));
      jButton159.setMargin(new Insets(0,0,0,0));
      jButton160.setMargin(new Insets(0,0,0,0));
      jButton161.setMargin(new Insets(0,0,0,0));
      jButton163.setMargin(new Insets(0,0,0,0));
      jButton164.setMargin(new Insets(0,0,0,0));
      jButton165.setMargin(new Insets(0,0,0,0));
      jButton166.setMargin(new Insets(0,0,0,0));
      jButton167.setMargin(new Insets(0,0,0,0));
      jButton168.setMargin(new Insets(0,0,0,0));
      jButton169.setMargin(new Insets(0,0,0,0));
      jButton170.setMargin(new Insets(0,0,0,0));
      jButton171.setMargin(new Insets(0,0,0,0));
      jButton172.setMargin(new Insets(0,0,0,0));
      jButton173.setMargin(new Insets(0,0,0,0));
      jButton174.setMargin(new Insets(0,0,0,0));
      jButton175.setMargin(new Insets(0,0,0,0));
      jButton176.setMargin(new Insets(0,0,0,0));
      jButton177.setMargin(new Insets(0,0,0,0));
      jButton178.setMargin(new Insets(0,0,0,0));
      jButton179.setMargin(new Insets(0,0,0,0));
      jButton180.setMargin(new Insets(0,0,0,0));
      jButton181.setMargin(new Insets(0,0,0,0));
      jButton182.setMargin(new Insets(0,0,0,0));
      jButton183.setMargin(new Insets(0,0,0,0));
      jButton184.setMargin(new Insets(0,0,0,0));
      jButton185.setMargin(new Insets(0,0,0,0));
      jButton186.setMargin(new Insets(0,0,0,0));
      jButton187.setMargin(new Insets(0,0,0,0));
      jButton188.setMargin(new Insets(0,0,0,0));
      jButton189.setMargin(new Insets(0,0,0,0));
      jButton190.setMargin(new Insets(0,0,0,0));
      jButton191.setMargin(new Insets(0,0,0,0));
      jButton192.setMargin(new Insets(0,0,0,0));
      jButton193.setMargin(new Insets(0,0,0,0));
      jButton194.setMargin(new Insets(0,0,0,0));
      jButton195.setMargin(new Insets(0,0,0,0));
      jButton196.setMargin(new Insets(0,0,0,0));
      jButton197.setMargin(new Insets(0,0,0,0));
      jButton198.setMargin(new Insets(0,0,0,0));
      jButton199.setMargin(new Insets(0,0,0,0));
      jButton200.setMargin(new Insets(0,0,0,0));
      jButton201.setMargin(new Insets(0,0,0,0));
      jButton202.setMargin(new Insets(0,0,0,0));
      jButton203.setMargin(new Insets(0,0,0,0));
      jButton204.setMargin(new Insets(0,0,0,0));
      jButton205.setMargin(new Insets(0,0,0,0));
      jButton206.setMargin(new Insets(0,0,0,0));
      jButton207.setMargin(new Insets(0,0,0,0));
      jButton208.setMargin(new Insets(0,0,0,0));
      jButton209.setMargin(new Insets(0,0,0,0));
      jButton210.setMargin(new Insets(0,0,0,0));
      jButton211.setMargin(new Insets(0,0,0,0));
      jButton212.setMargin(new Insets(0,0,0,0));
      jButton213.setMargin(new Insets(0,0,0,0));
      jButton214.setMargin(new Insets(0,0,0,0));
      jButton215.setMargin(new Insets(0,0,0,0));
      jButton216.setMargin(new Insets(0,0,0,0));
      jButton217.setMargin(new Insets(0,0,0,0));
      jButton218.setMargin(new Insets(0,0,0,0));
      jButton219.setMargin(new Insets(0,0,0,0));
      jButton220.setMargin(new Insets(0,0,0,0));
      jButton221.setMargin(new Insets(0,0,0,0));
      jButton222.setMargin(new Insets(0,0,0,0));
      jButton223.setMargin(new Insets(0,0,0,0));
      jButton224.setMargin(new Insets(0,0,0,0));
      jButton225.setMargin(new Insets(0,0,0,0));
      jButton226.setMargin(new Insets(0,0,0,0));
      jButton228.setMargin(new Insets(0,0,0,0));
      jButton229.setMargin(new Insets(0,0,0,0));
      jButton231.setMargin(new Insets(0,0,0,0));
      jButton232.setMargin(new Insets(0,0,0,0));
      jButton233.setMargin(new Insets(0,0,0,0));
      jButton234.setMargin(new Insets(0,0,0,0));
      jButton235.setMargin(new Insets(0,0,0,0));
      jButton236.setMargin(new Insets(0,0,0,0));
      jButton237.setMargin(new Insets(0,0,0,0));
      jButton238.setMargin(new Insets(0,0,0,0));
      jButton239.setMargin(new Insets(0,0,0,0));
      jButton240.setMargin(new Insets(0,0,0,0));
      jButton241.setMargin(new Insets(0,0,0,0));
      jButton242.setMargin(new Insets(0,0,0,0));
      jButton243.setMargin(new Insets(0,0,0,0));
      jButton244.setMargin(new Insets(0,0,0,0));
      jButton245.setMargin(new Insets(0,0,0,0));
      jButton246.setMargin(new Insets(0,0,0,0));
      jButton247.setMargin(new Insets(0,0,0,0));
      jButton248.setMargin(new Insets(0,0,0,0));
      jButton249.setMargin(new Insets(0,0,0,0));
      jButton250.setMargin(new Insets(0,0,0,0));
      jButton251.setMargin(new Insets(0,0,0,0));
      jButton252.setMargin(new Insets(0,0,0,0));
      jButton253.setMargin(new Insets(0,0,0,0));
      jButton254.setMargin(new Insets(0,0,0,0));
      jButton255.setMargin(new Insets(0,0,0,0));
      jButton256.setMargin(new Insets(0,0,0,0));
      jButton257.setMargin(new Insets(0,0,0,0));
      jButton258.setMargin(new Insets(0,0,0,0));
      jButton259.setMargin(new Insets(0,0,0,0));
      jButton260.setMargin(new Insets(0,0,0,0));
      jButton261.setMargin(new Insets(0,0,0,0));
      jButton262.setMargin(new Insets(0,0,0,0));
      jButton263.setMargin(new Insets(0,0,0,0));
      jButton264.setMargin(new Insets(0,0,0,0));
      jButton265.setMargin(new Insets(0,0,0,0));
      jButton266.setMargin(new Insets(0,0,0,0));
      jButton267.setMargin(new Insets(0,0,0,0));
      jButton268.setMargin(new Insets(0,0,0,0));
      jButton269.setMargin(new Insets(0,0,0,0));
      jButton270.setMargin(new Insets(0,0,0,0));
      jButton271.setMargin(new Insets(0,0,0,0));
      jButton272.setMargin(new Insets(0,0,0,0));
      jButton273.setMargin(new Insets(0,0,0,0));
      jButton274.setMargin(new Insets(0,0,0,0));
      jButton275.setMargin(new Insets(0,0,0,0));
      jButton276.setMargin(new Insets(0,0,0,0));
      jButton277.setMargin(new Insets(0,0,0,0));
      jButton278.setMargin(new Insets(0,0,0,0));
      jButton279.setMargin(new Insets(0,0,0,0));
      jButton280.setMargin(new Insets(0,0,0,0));
      jButton281.setMargin(new Insets(0,0,0,0));
      jButton282.setMargin(new Insets(0,0,0,0));
      jButton284.setMargin(new Insets(0,0,0,0));
      jButton285.setMargin(new Insets(0,0,0,0));
      jButton286.setMargin(new Insets(0,0,0,0));
      jButton287.setMargin(new Insets(0,0,0,0));
      jButton288.setMargin(new Insets(0,0,0,0));
      jButton289.setMargin(new Insets(0,0,0,0));
      jButton290.setMargin(new Insets(0,0,0,0));
      jButton291.setMargin(new Insets(0,0,0,0));
      jButton292.setMargin(new Insets(0,0,0,0));
      jButton293.setMargin(new Insets(0,0,0,0));
      jButton294.setMargin(new Insets(0,0,0,0));
      jButton295.setMargin(new Insets(0,0,0,0));
      jButton296.setMargin(new Insets(0,0,0,0));
      jButton298.setMargin(new Insets(0,0,0,0));
      jButton299.setMargin(new Insets(0,0,0,0));
      jButton300.setMargin(new Insets(0,0,0,0));
      jButton301.setMargin(new Insets(0,0,0,0));
      jButton302.setMargin(new Insets(0,0,0,0));
      jButton303.setMargin(new Insets(0,0,0,0));
      jButton304.setMargin(new Insets(0,0,0,0));
      jButton305.setMargin(new Insets(0,0,0,0));
      jButton307.setMargin(new Insets(0,0,0,0));
      jButton308.setMargin(new Insets(0,0,0,0));
      jButton309.setMargin(new Insets(0,0,0,0));
      jButton311.setMargin(new Insets(0,0,0,0));
      jButton312.setMargin(new Insets(0,0,0,0));
      jButton313.setMargin(new Insets(0,0,0,0));
      jButton314.setMargin(new Insets(0,0,0,0));
      jButton315.setMargin(new Insets(0,0,0,0));
      jButton316.setMargin(new Insets(0,0,0,0));
      jButton317.setMargin(new Insets(0,0,0,0));
      jButton318.setMargin(new Insets(0,0,0,0));
      
      jButton320.setMargin(new Insets(0,0,0,0));
      jButton321.setMargin(new Insets(0,0,0,0));
      
      jButton323.setMargin(new Insets(0,0,0,0));
      jButton324.setMargin(new Insets(0,0,0,0));
      
      jButton326.setMargin(new Insets(0,0,0,0));
      jButton327.setMargin(new Insets(0,0,0,0));
      jButton328.setMargin(new Insets(0,0,0,0));
      
      jButton330.setMargin(new Insets(0,0,0,0));
      jButton331.setMargin(new Insets(0,0,0,0));
      jButton332.setMargin(new Insets(0,0,0,0));
      jButton333.setMargin(new Insets(0,0,0,0));
      jButton334.setMargin(new Insets(0,0,0,0));
      jButton335.setMargin(new Insets(0,0,0,0));
      
      jButton337.setMargin(new Insets(0,0,0,0));
      jButton338.setMargin(new Insets(0,0,0,0));
      jButton339.setMargin(new Insets(0,0,0,0));
      jButton340.setMargin(new Insets(0,0,0,0));
      jButton341.setMargin(new Insets(0,0,0,0));
      
      jButton343.setMargin(new Insets(0,0,0,0));
      jButton344.setMargin(new Insets(0,0,0,0));
      
      
      jButton347.setMargin(new Insets(0,0,0,0));
      jButton348.setMargin(new Insets(0,0,0,0));
      
      jButton350.setMargin(new Insets(0,0,0,0));
      jButton351.setMargin(new Insets(0,0,0,0));
      jButton352.setMargin(new Insets(0,0,0,0));
      jButton353.setMargin(new Insets(0,0,0,0));
      jButton354.setMargin(new Insets(0,0,0,0));
      
      jButton357.setMargin(new Insets(0,0,0,0));
      jButton358.setMargin(new Insets(0,0,0,0));
      jButton359.setMargin(new Insets(0,0,0,0));
      jButton360.setMargin(new Insets(0,0,0,0));
      
      jButton362.setMargin(new Insets(0,0,0,0));
      jButton363.setMargin(new Insets(0,0,0,0));
      jButton364.setMargin(new Insets(0,0,0,0));
      jButton365.setMargin(new Insets(0,0,0,0));
      jButton366.setMargin(new Insets(0,0,0,0));
      jButton367.setMargin(new Insets(0,0,0,0));
      
      jButton369.setMargin(new Insets(0,0,0,0));
      jButton370.setMargin(new Insets(0,0,0,0));
      
      jButton372.setMargin(new Insets(0,0,0,0));
      jButton373.setMargin(new Insets(0,0,0,0));
      
      jButton375.setMargin(new Insets(0,0,0,0));
      jButton376.setMargin(new Insets(0,0,0,0));
      jButton377.setMargin(new Insets(0,0,0,0));
      
      jButton379.setMargin(new Insets(0,0,0,0));
      jButton380.setMargin(new Insets(0,0,0,0));
      
      jButton382.setMargin(new Insets(0,0,0,0));
      jButton383.setMargin(new Insets(0,0,0,0));
      
      jButton385.setMargin(new Insets(0,0,0,0));
      jButton386.setMargin(new Insets(0,0,0,0));
      jButton387.setMargin(new Insets(0,0,0,0));
      
      jButton389.setMargin(new Insets(0,0,0,0));
      jButton390.setMargin(new Insets(0,0,0,0));
      jButton391.setMargin(new Insets(0,0,0,0));
      jButton392.setMargin(new Insets(0,0,0,0));
      jButton393.setMargin(new Insets(0,0,0,0));
      jButton394.setMargin(new Insets(0,0,0,0));
      jButton395.setMargin(new Insets(0,0,0,0));
      jButton396.setMargin(new Insets(0,0,0,0));
      jButton397.setMargin(new Insets(0,0,0,0));
      jButton398.setMargin(new Insets(0,0,0,0));
      jButton399.setMargin(new Insets(0,0,0,0));
      jButton400.setMargin(new Insets(0,0,0,0));
      jButton401.setMargin(new Insets(0,0,0,0));
      jButton402.setMargin(new Insets(0,0,0,0));
      jButton403.setMargin(new Insets(0,0,0,0));
      jButton404.setMargin(new Insets(0,0,0,0));
      jButton405.setMargin(new Insets(0,0,0,0));
      jButton406.setMargin(new Insets(0,0,0,0));
      jButton407.setMargin(new Insets(0,0,0,0));
      jButton408.setMargin(new Insets(0,0,0,0));
      jButton409.setMargin(new Insets(0,0,0,0));
      jButton410.setMargin(new Insets(0,0,0,0));
      jButton411.setMargin(new Insets(0,0,0,0));
      jButton412.setMargin(new Insets(0,0,0,0));
      jButton413.setMargin(new Insets(0,0,0,0));
      jButton414.setMargin(new Insets(0,0,0,0));
      
      
      
      
      
      
      
      
      
      
   
      jButton8.setMaximumSize(new java.awt.Dimension(25,25));
      jButton8.setMinimumSize(new java.awt.Dimension(25,25));
      jButton8.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 5;
      jButton8.setText("28");
      top.add(jButton8, gridBagConstraints);

      jButton9.setMaximumSize(new java.awt.Dimension(25,25));
      jButton9.setMinimumSize(new java.awt.Dimension(25,25));
      jButton9.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 2;
      jButton9.setText("7");
      top.add(jButton9, gridBagConstraints);

      jButton10.setMaximumSize(new java.awt.Dimension(25,25));
      jButton10.setMinimumSize(new java.awt.Dimension(25,25));
      jButton10.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 8;
      jButton10.setText("65");
      top.add(jButton10, gridBagConstraints);

      jButton11.setMaximumSize(new java.awt.Dimension(25,25));
      jButton11.setMinimumSize(new java.awt.Dimension(25,25));
      jButton11.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 6;
      jButton11.setText("39");
      top.add(jButton11, gridBagConstraints);

      jButton12.setMaximumSize(new java.awt.Dimension(25,25));
      jButton12.setMinimumSize(new java.awt.Dimension(25,25));
      jButton12.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 1;
      jButton12.setText("4");
      top.add(jButton12, gridBagConstraints);

      jButton13.setMaximumSize(new java.awt.Dimension(25,25));
      jButton13.setMinimumSize(new java.awt.Dimension(25,25));
      jButton13.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 0;
      gridBagConstraints.gridy = 18;
      jButton13.setText("154");
      top.add(jButton13, gridBagConstraints);

      jButton14.setMaximumSize(new java.awt.Dimension(25,25));
      jButton14.setMinimumSize(new java.awt.Dimension(25,25));
      jButton14.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 8;
      jButton14.setText("67");
      top.add(jButton14, gridBagConstraints);

      jButton15.setMaximumSize(new java.awt.Dimension(25,25));
      jButton15.setMinimumSize(new java.awt.Dimension(25,25));
      jButton15.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 2;
      jButton15.setText("9");
      top.add(jButton15, gridBagConstraints);

      jButton16.setMaximumSize(new java.awt.Dimension(25,25));
      jButton16.setMinimumSize(new java.awt.Dimension(25,25));
      jButton16.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 20;
      jButton16.setText("170");
      top.add(jButton16, gridBagConstraints);

      jButton17.setMaximumSize(new java.awt.Dimension(25,25));
      jButton17.setMinimumSize(new java.awt.Dimension(25,25));
      jButton17.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 11;
      jButton17.setText("93");
      top.add(jButton17, gridBagConstraints);

      jButton18.setMaximumSize(new java.awt.Dimension(25,25));
      jButton18.setMinimumSize(new java.awt.Dimension(25,25));
      jButton18.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 17;
      jButton18.setText("145");
      top.add(jButton18, gridBagConstraints);

      jButton19.setMaximumSize(new java.awt.Dimension(25,25));
      jButton19.setMinimumSize(new java.awt.Dimension(25,25));
      jButton19.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 22;
      gridBagConstraints.gridy = 16;
      jButton19.setText("138");
      top.add(jButton19, gridBagConstraints);

      jButton20.setMaximumSize(new java.awt.Dimension(25,25));
      jButton20.setMinimumSize(new java.awt.Dimension(25,25));
      jButton20.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 6;
      jButton20.setText("41");
      top.add(jButton20, gridBagConstraints);

      jButton21.setMaximumSize(new java.awt.Dimension(25,25));
      jButton21.setMinimumSize(new java.awt.Dimension(25,25));
      jButton21.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 11;
      jButton21.setText("90");
      top.add(jButton21, gridBagConstraints);

      jButton22.setMaximumSize(new java.awt.Dimension(25,25));
      jButton22.setMinimumSize(new java.awt.Dimension(25,25));
      jButton22.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 1;
      jButton22.setText("3");
      top.add(jButton22, gridBagConstraints);

      jButton23.setMaximumSize(new java.awt.Dimension(25,25));
      jButton23.setMinimumSize(new java.awt.Dimension(25,25));
      jButton23.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 5;
      jButton23.setText("34");
      top.add(jButton23, gridBagConstraints);

      jButton24.setMaximumSize(new java.awt.Dimension(25,25));
      jButton24.setMinimumSize(new java.awt.Dimension(25,25));
      jButton24.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 23;
      jButton24.setText("182");
      top.add(jButton24, gridBagConstraints);

      jButton26.setMaximumSize(new java.awt.Dimension(25,25));
      jButton26.setMinimumSize(new java.awt.Dimension(25,25));
      jButton26.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 21;
      gridBagConstraints.gridy = 6;
      jButton26.setText("45");
      top.add(jButton26, gridBagConstraints);

      jButton28.setMaximumSize(new java.awt.Dimension(25,25));
      jButton28.setMinimumSize(new java.awt.Dimension(25,25));
      jButton28.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 22;
      gridBagConstraints.gridy = 7;
      jButton28.setText("62");
      top.add(jButton28, gridBagConstraints);

      jButton29.setMaximumSize(new java.awt.Dimension(25,25));
      jButton29.setMinimumSize(new java.awt.Dimension(25,25));
      jButton29.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 13;
      gridBagConstraints.gridy = 7;
      jButton29.setText("53");
      top.add(jButton29, gridBagConstraints);

      jButton30.setMaximumSize(new java.awt.Dimension(25,25));
      jButton30.setMinimumSize(new java.awt.Dimension(25,25));
      jButton30.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 7;
      jButton30.setText("48");
      top.add(jButton30, gridBagConstraints);

      jButton31.setMaximumSize(new java.awt.Dimension(25,25));
      jButton31.setMinimumSize(new java.awt.Dimension(25,25));
      jButton31.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 9;
      jButton31.setText("75");
      top.add(jButton31, gridBagConstraints);

      jButton32.setMaximumSize(new java.awt.Dimension(25,25));
      jButton32.setMinimumSize(new java.awt.Dimension(25,25));
      jButton32.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 5;
      jButton32.setText("27");
      top.add(jButton32, gridBagConstraints);

      jButton33.setMaximumSize(new java.awt.Dimension(25,25));
      jButton33.setMinimumSize(new java.awt.Dimension(25,25));
      jButton33.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 16;
      jButton33.setText("132");
      top.add(jButton33, gridBagConstraints);

      jButton35.setMaximumSize(new java.awt.Dimension(25,25));
      jButton35.setMinimumSize(new java.awt.Dimension(25,25));
      jButton35.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 4;
      jButton35.setText("17");
      top.add(jButton35, gridBagConstraints);

      jButton36.setMaximumSize(new java.awt.Dimension(25,25));
      jButton36.setMinimumSize(new java.awt.Dimension(25,25));
      jButton36.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 18;
      jButton36.setText("161");
      top.add(jButton36, gridBagConstraints);

      jButton37.setMaximumSize(new java.awt.Dimension(25,25));
      jButton37.setMinimumSize(new java.awt.Dimension(25,25));
      jButton37.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 22;
      gridBagConstraints.gridy = 6;
      jButton37.setText("46");
      top.add(jButton37, gridBagConstraints);

      jButton38.setMaximumSize(new java.awt.Dimension(25,25));
      jButton38.setMinimumSize(new java.awt.Dimension(25,25));
      jButton38.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 21;
      jButton38.setText("173");
      top.add(jButton38, gridBagConstraints);

      

      jButton40.setMaximumSize(new java.awt.Dimension(25,25));
      jButton40.setMinimumSize(new java.awt.Dimension(25,25));
      jButton40.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 2;
      jButton40.setText("8");
      top.add(jButton40, gridBagConstraints);

      jButton41.setMaximumSize(new java.awt.Dimension(25,25));
      jButton41.setMinimumSize(new java.awt.Dimension(25,25));
      jButton41.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 22;
      gridBagConstraints.gridy = 17;
      jButton41.setText("152");
      top.add(jButton41, gridBagConstraints);

      jButton42.setMaximumSize(new java.awt.Dimension(25,25));
      jButton42.setMinimumSize(new java.awt.Dimension(25,25));
      jButton42.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 4;
      jButton42.setText("20");
      top.add(jButton42, gridBagConstraints);

      jButton43.setMaximumSize(new java.awt.Dimension(25,25));
      jButton43.setMinimumSize(new java.awt.Dimension(25,25));
      jButton43.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 5;
      jButton43.setText("31");
      top.add(jButton43, gridBagConstraints);

      jButton44.setMaximumSize(new java.awt.Dimension(25,25));
      jButton44.setMinimumSize(new java.awt.Dimension(25,25));
      jButton44.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 14;
      jButton44.setText("108");
      top.add(jButton44, gridBagConstraints);

      jButton47.setMaximumSize(new java.awt.Dimension(25,25));
      jButton47.setMinimumSize(new java.awt.Dimension(25,25));
      jButton47.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 11;
      jButton47.setText("88");
      top.add(jButton47, gridBagConstraints);

      jButton48.setMaximumSize(new java.awt.Dimension(25,25));
      jButton48.setMinimumSize(new java.awt.Dimension(25,25));
      jButton48.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 17;
      jButton48.setText("141");
      top.add(jButton48, gridBagConstraints);

      jButton49.setMaximumSize(new java.awt.Dimension(25,25));
      jButton49.setMinimumSize(new java.awt.Dimension(25,25));
      jButton49.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 18;
      jButton49.setText("159");
      top.add(jButton49, gridBagConstraints);

      jButton50.setMaximumSize(new java.awt.Dimension(25,25));
      jButton50.setMinimumSize(new java.awt.Dimension(25,25));
      jButton50.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 13;
      gridBagConstraints.gridy = 15;
      jButton50.setText("116");
      top.add(jButton50, gridBagConstraints);

      jButton51.setMaximumSize(new java.awt.Dimension(25,25));
      jButton51.setMinimumSize(new java.awt.Dimension(25,25));
      jButton51.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 22;
      jButton51.setText("178");
      top.add(jButton51, gridBagConstraints);

      jButton53.setMaximumSize(new java.awt.Dimension(25,25));
      jButton53.setMinimumSize(new java.awt.Dimension(25,25));
      jButton53.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 5;
      jButton53.setText("29");
      top.add(jButton53, gridBagConstraints);

      jButton54.setMaximumSize(new java.awt.Dimension(25,25));
      jButton54.setMinimumSize(new java.awt.Dimension(25,25));
      jButton54.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 4;
      jButton54.setText("15");
      top.add(jButton54, gridBagConstraints);

      jButton55.setMaximumSize(new java.awt.Dimension(25,25));
      jButton55.setMinimumSize(new java.awt.Dimension(25,25));
      jButton55.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 23;
      gridBagConstraints.gridy = 7;
      jButton55.setText("63");
      top.add(jButton55, gridBagConstraints);

      jButton56.setMaximumSize(new java.awt.Dimension(25,25));
      jButton56.setMinimumSize(new java.awt.Dimension(25,25));
      jButton56.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 5;
      jButton56.setText("32");
      top.add(jButton56, gridBagConstraints);

      jButton57.setMaximumSize(new java.awt.Dimension(25,25));
      jButton57.setMinimumSize(new java.awt.Dimension(25,25));
      jButton57.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 7;
      jButton57.setText("55");
      top.add(jButton57, gridBagConstraints);

      jButton58.setMaximumSize(new java.awt.Dimension(25,25));
      jButton58.setMinimumSize(new java.awt.Dimension(25,25));
      jButton58.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 16;
      jButton58.setText("124");
      top.add(jButton58, gridBagConstraints);

      jButton59.setMaximumSize(new java.awt.Dimension(25,25));
      jButton59.setMinimumSize(new java.awt.Dimension(25,25));
      jButton59.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 5;
      jButton59.setText("26");
      top.add(jButton59, gridBagConstraints);

      jButton62.setMaximumSize(new java.awt.Dimension(25,25));
      jButton62.setMinimumSize(new java.awt.Dimension(25,25));
      jButton62.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 0;
      gridBagConstraints.gridy = 5;
      jButton62.setText("25");
      top.add(jButton62, gridBagConstraints);

      jButton63.setMaximumSize(new java.awt.Dimension(25,25));
      jButton63.setMinimumSize(new java.awt.Dimension(25,25));
      jButton63.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 3;
      jButton63.setText("11");
      top.add(jButton63, gridBagConstraints);

      jButton64.setMaximumSize(new java.awt.Dimension(25,25));
      jButton64.setMinimumSize(new java.awt.Dimension(25,25));
      jButton64.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 15;
      jButton64.setText("119");
      top.add(jButton64, gridBagConstraints);

      jButton65.setMaximumSize(new java.awt.Dimension(25,25));
      jButton65.setMinimumSize(new java.awt.Dimension(25,25));
      jButton65.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 3;
      jButton65.setText("12");
      top.add(jButton65, gridBagConstraints);

      


      jButton68.setMaximumSize(new java.awt.Dimension(25,25));
      jButton68.setMinimumSize(new java.awt.Dimension(25,25));
      jButton68.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 23;
      gridBagConstraints.gridy = 17;
      jButton68.setText("153");
      top.add(jButton68, gridBagConstraints);

      

      jButton70.setMaximumSize(new java.awt.Dimension(25,25));
      jButton70.setMinimumSize(new java.awt.Dimension(25,25));
      jButton70.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 5;
      jButton70.setText("31");
      top.add(jButton70, gridBagConstraints);

      jButton71.setMaximumSize(new java.awt.Dimension(25,25));
      jButton71.setMinimumSize(new java.awt.Dimension(25,25));
      jButton71.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 0;
      jButton71.setText("2");
      top.add(jButton71, gridBagConstraints);

      jButton72.setMaximumSize(new java.awt.Dimension(25,25));
      jButton72.setMinimumSize(new java.awt.Dimension(25,25));
      jButton72.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 4;
      jButton72.setText("23");
      top.add(jButton72, gridBagConstraints);

      jButton73.setMaximumSize(new java.awt.Dimension(25,25));
      jButton73.setMinimumSize(new java.awt.Dimension(25,25));
      jButton73.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 3;
      jButton73.setText("14");
      top.add(jButton73, gridBagConstraints);

      jButton74.setMaximumSize(new java.awt.Dimension(25,25));
      jButton74.setMinimumSize(new java.awt.Dimension(25,25));
      jButton74.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 5;
      jButton74.setText("35");
      top.add(jButton74, gridBagConstraints);

      jButton75.setMaximumSize(new java.awt.Dimension(25,25));
      jButton75.setMinimumSize(new java.awt.Dimension(25,25));
      jButton75.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 5;
      jButton75.setText("30");
      top.add(jButton75, gridBagConstraints);

      jButton76.setMaximumSize(new java.awt.Dimension(25,25));
      jButton76.setMinimumSize(new java.awt.Dimension(25,25));
      jButton76.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 0;
      jButton76.setText("1");
      top.add(jButton76, gridBagConstraints);

      jButton77.setMaximumSize(new java.awt.Dimension(25,25));
      jButton77.setMinimumSize(new java.awt.Dimension(25,25));
      jButton77.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 9;
      gridBagConstraints.gridy = 24;
      jButton77.setText("187");
      top.add(jButton77, gridBagConstraints);

      jButton78.setMaximumSize(new java.awt.Dimension(25,25));
      jButton78.setMinimumSize(new java.awt.Dimension(25,25));
      jButton78.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 20;
      jButton78.setText("169");
      top.add(jButton78, gridBagConstraints);

      jButton80.setBackground(Color.BLACK);
      jButton80.setForeground(Color.WHITE);
      jButton80.setMaximumSize(new java.awt.Dimension(25,25));
      jButton80.setMinimumSize(new java.awt.Dimension(25,25));
      jButton80.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 17;
      jButton80.setText("192");
      top.add(jButton80, gridBagConstraints);

      jButton81.setMaximumSize(new java.awt.Dimension(25,25));
      jButton81.setMinimumSize(new java.awt.Dimension(25,25));
      jButton81.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 14;
      jButton81.setText("107");
      top.add(jButton81, gridBagConstraints);

      jButton82.setMaximumSize(new java.awt.Dimension(25,25));
      jButton82.setMinimumSize(new java.awt.Dimension(25,25));
      jButton82.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 1;
      jButton82.setText("5");
      top.add(jButton82, gridBagConstraints);

      jButton83.setMaximumSize(new java.awt.Dimension(25,25));
      jButton83.setMinimumSize(new java.awt.Dimension(25,25));
      jButton83.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 5;
      jButton83.setText("32");
      top.add(jButton83, gridBagConstraints);

      jButton84.setMaximumSize(new java.awt.Dimension(25,25));
      jButton84.setMinimumSize(new java.awt.Dimension(25,25));
      jButton84.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 7;
      jButton84.setText("56");
      top.add(jButton84, gridBagConstraints);

      jButton85.setMaximumSize(new java.awt.Dimension(25,25));
      jButton85.setMinimumSize(new java.awt.Dimension(25,25));
      jButton85.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 8;
      jButton85.setText("66");
      top.add(jButton85, gridBagConstraints);

      jButton86.setMaximumSize(new java.awt.Dimension(25,25));
      jButton86.setMinimumSize(new java.awt.Dimension(25,25));
      jButton86.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 5;
      jButton86.setText("27");
      top.add(jButton86, gridBagConstraints);

      jButton88.setMaximumSize(new java.awt.Dimension(25,25));
      jButton88.setMinimumSize(new java.awt.Dimension(25,25));
      jButton88.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 11;
      jButton88.setText("91");
      top.add(jButton88, gridBagConstraints);

      jButton89.setMaximumSize(new java.awt.Dimension(25,25));
      jButton89.setMinimumSize(new java.awt.Dimension(25,25));
      jButton89.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 12;
      gridBagConstraints.gridy = 16;
      jButton89.setText("128");
      top.add(jButton89, gridBagConstraints);

      jButton90.setMaximumSize(new java.awt.Dimension(25,25));
      jButton90.setMinimumSize(new java.awt.Dimension(25,25));
      jButton90.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 18;
      gridBagConstraints.gridy = 6;
      jButton90.setText("42");
      top.add(jButton90, gridBagConstraints);

      jButton91.setMaximumSize(new java.awt.Dimension(25,25));
      jButton91.setMinimumSize(new java.awt.Dimension(25,25));
      jButton91.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 9;
      gridBagConstraints.gridy = 7;
      jButton91.setText("49");
      top.add(jButton91, gridBagConstraints);

      jButton92.setMaximumSize(new java.awt.Dimension(25,25));
      jButton92.setMinimumSize(new java.awt.Dimension(25,25));
      jButton92.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 13;
      jButton92.setText("102");
      top.add(jButton92, gridBagConstraints);

      jButton94.setMaximumSize(new java.awt.Dimension(25,25));
      jButton94.setMinimumSize(new java.awt.Dimension(25,25));
      jButton94.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 11;
      jButton94.setText("89");
      top.add(jButton94, gridBagConstraints);

      jButton95.setMaximumSize(new java.awt.Dimension(25,25));
      jButton95.setMinimumSize(new java.awt.Dimension(25,25));
      jButton95.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 9;
      gridBagConstraints.gridy = 23;
      jButton95.setText("183");
      top.add(jButton95, gridBagConstraints);

      jButton96.setMaximumSize(new java.awt.Dimension(25,25));
      jButton96.setMinimumSize(new java.awt.Dimension(25,25));
      jButton96.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 21;
      gridBagConstraints.gridy = 7;
      jButton96.setText("61");
      top.add(jButton96, gridBagConstraints);

      jButton97.setMaximumSize(new java.awt.Dimension(25,25));
      jButton97.setMinimumSize(new java.awt.Dimension(25,25));
      jButton97.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 13;
      jButton97.setText("99");
      top.add(jButton97, gridBagConstraints);

      jButton98.setMaximumSize(new java.awt.Dimension(25,25));
      jButton98.setMinimumSize(new java.awt.Dimension(25,25));
      jButton98.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 9;
      jButton98.setText("78");
      top.add(jButton98, gridBagConstraints);

      jButton99.setMaximumSize(new java.awt.Dimension(25,25));
      jButton99.setMinimumSize(new java.awt.Dimension(25,25));
      jButton99.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 18;
      jButton99.setText("155");
      top.add(jButton99, gridBagConstraints);

      jButton101.setMaximumSize(new java.awt.Dimension(25,25));
      jButton101.setMinimumSize(new java.awt.Dimension(25,25));
      jButton101.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 18;
      jButton101.setText("160");
      top.add(jButton101, gridBagConstraints);

      jButton102.setMaximumSize(new java.awt.Dimension(25,25));
      jButton102.setMinimumSize(new java.awt.Dimension(25,25));
      jButton102.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 12;
      gridBagConstraints.gridy = 15;
      jButton102.setText("115");
      top.add(jButton102, gridBagConstraints);

      jButton103.setMaximumSize(new java.awt.Dimension(25,25));
      jButton103.setMinimumSize(new java.awt.Dimension(25,25));
      jButton103.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 8;
      jButton103.setText("68");
      top.add(jButton103, gridBagConstraints);

      jButton104.setMaximumSize(new java.awt.Dimension(25,25));
      jButton104.setMinimumSize(new java.awt.Dimension(25,25));
      jButton104.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 11;
      jButton104.setText("84");
      top.add(jButton104, gridBagConstraints);

      

      jButton106.setMaximumSize(new java.awt.Dimension(25,25));
      jButton106.setMinimumSize(new java.awt.Dimension(25,25));
      jButton106.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 8;
      jButton106.setText("64");
      top.add(jButton106, gridBagConstraints);

      jButton107.setMaximumSize(new java.awt.Dimension(25,25));
      jButton107.setMinimumSize(new java.awt.Dimension(25,25));
      jButton107.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 15;
      jButton107.setText("110");
      top.add(jButton107, gridBagConstraints);

      jButton108.setMaximumSize(new java.awt.Dimension(25,25));
      jButton108.setMinimumSize(new java.awt.Dimension(25,25));
      jButton108.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 14;
      jButton108.setText("106");
      top.add(jButton108, gridBagConstraints);

      jButton109.setMaximumSize(new java.awt.Dimension(25,25));
      jButton109.setMinimumSize(new java.awt.Dimension(25,25));
      jButton109.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 17;
      jButton109.setText("143");
      top.add(jButton109, gridBagConstraints);

      jButton110.setMaximumSize(new java.awt.Dimension(25,25));
      jButton110.setMinimumSize(new java.awt.Dimension(25,25));
      jButton110.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 16;
      jButton110.setText("130");
      top.add(jButton110, gridBagConstraints);

      jButton111.setMaximumSize(new java.awt.Dimension(25,25));
      jButton111.setMinimumSize(new java.awt.Dimension(25,25));
      jButton111.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 22;
      jButton111.setText("180");
      top.add(jButton111, gridBagConstraints);

      jButton112.setMaximumSize(new java.awt.Dimension(25,25));
      jButton112.setMinimumSize(new java.awt.Dimension(25,25));
      jButton112.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 13;
      jButton112.setText("100");
      top.add(jButton112, gridBagConstraints);

      jButton113.setMaximumSize(new java.awt.Dimension(25,25));
      jButton113.setMinimumSize(new java.awt.Dimension(25,25));
      jButton113.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 12;
      jButton113.setText("96");
      top.add(jButton113, gridBagConstraints);

      jButton114.setMaximumSize(new java.awt.Dimension(25,25));
      jButton114.setMinimumSize(new java.awt.Dimension(25,25));
      jButton114.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 21;
      jButton114.setText("174");
      top.add(jButton114, gridBagConstraints);

      jButton115.setMaximumSize(new java.awt.Dimension(25,25));
      jButton115.setMinimumSize(new java.awt.Dimension(25,25));
      jButton115.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 10;
      jButton115.setText("81");
      top.add(jButton115, gridBagConstraints);

      jButton116.setMaximumSize(new java.awt.Dimension(25,25));
      jButton116.setMinimumSize(new java.awt.Dimension(25,25));
      jButton116.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 17;
      jButton116.setText("139");
      top.add(jButton116, gridBagConstraints);

      jButton117.setMaximumSize(new java.awt.Dimension(25,25));
      jButton117.setMinimumSize(new java.awt.Dimension(25,25));
      jButton117.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 13;
      gridBagConstraints.gridy = 16;
      jButton117.setText("129");
      top.add(jButton117, gridBagConstraints);

      jButton118.setMaximumSize(new java.awt.Dimension(25,25));
      jButton118.setMinimumSize(new java.awt.Dimension(25,25));
      jButton118.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 11;
      jButton118.setText("87");
      top.add(jButton118, gridBagConstraints);

      jButton119.setMaximumSize(new java.awt.Dimension(25,25));
      jButton119.setMinimumSize(new java.awt.Dimension(25,25));
      jButton119.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 19;
      jButton119.setText("164");
      top.add(jButton119, gridBagConstraints);

      jButton120.setMaximumSize(new java.awt.Dimension(25,25));
      jButton120.setMinimumSize(new java.awt.Dimension(25,25));
      jButton120.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 19;
      gridBagConstraints.gridy = 6;
      jButton120.setText("43");
      top.add(jButton120, gridBagConstraints);

      

      jButton123.setMaximumSize(new java.awt.Dimension(25,25));
      jButton123.setMinimumSize(new java.awt.Dimension(25,25));
      jButton123.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 17;
      jButton123.setText("144");
      top.add(jButton123, gridBagConstraints);

      jButton124.setMaximumSize(new java.awt.Dimension(25,25));
      jButton124.setMinimumSize(new java.awt.Dimension(25,25));
      jButton124.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 20;
      gridBagConstraints.gridy = 7;
      jButton124.setText("60");
      top.add(jButton124, gridBagConstraints);

      jButton125.setMaximumSize(new java.awt.Dimension(25,25));
      jButton125.setMinimumSize(new java.awt.Dimension(25,25));
      jButton125.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 23;
      jButton125.setText("185");
      top.add(jButton125, gridBagConstraints);

      
      jButton127.setMaximumSize(new java.awt.Dimension(25,25));
      jButton127.setMinimumSize(new java.awt.Dimension(25,25));
      jButton127.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 23;
      jButton127.setText("181");
      top.add(jButton127, gridBagConstraints);

      jButton128.setMaximumSize(new java.awt.Dimension(25,25));
      jButton128.setMinimumSize(new java.awt.Dimension(25,25));
      jButton128.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 18;
      jButton128.setText("156");
      top.add(jButton128, gridBagConstraints);

      jButton129.setMaximumSize(new java.awt.Dimension(25,25));
      jButton129.setMinimumSize(new java.awt.Dimension(25,25));
      jButton129.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 19;
      jButton129.setText("165");
      top.add(jButton129, gridBagConstraints);

      jButton130.setMaximumSize(new java.awt.Dimension(25,25));
      jButton130.setMinimumSize(new java.awt.Dimension(25,25));
      jButton130.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 11;
      jButton130.setText("85");
      top.add(jButton130, gridBagConstraints);

      jButton131.setMaximumSize(new java.awt.Dimension(25,25));
      jButton131.setMinimumSize(new java.awt.Dimension(25,25));
      jButton131.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 17;
      jButton131.setText("140");
      top.add(jButton131, gridBagConstraints);

      jButton132.setMaximumSize(new java.awt.Dimension(25,25));
      jButton132.setMinimumSize(new java.awt.Dimension(25,25));
      jButton132.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 11;
      top.add(jButton132, gridBagConstraints);

     
      jButton134.setMaximumSize(new java.awt.Dimension(25,25));
      jButton134.setMinimumSize(new java.awt.Dimension(25,25));
      jButton134.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 17;
      jButton134.setText("142");
      top.add(jButton134, gridBagConstraints);

      jButton135.setMaximumSize(new java.awt.Dimension(25,25));
      jButton135.setMinimumSize(new java.awt.Dimension(25,25));
      jButton135.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 18;
      jButton135.setText("158");
      top.add(jButton135, gridBagConstraints);

      jButton136.setMaximumSize(new java.awt.Dimension(25,25));
      jButton136.setMinimumSize(new java.awt.Dimension(25,25));
      jButton136.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 10;
      jButton136.setText("83");
      top.add(jButton136, gridBagConstraints);

      jButton137.setMaximumSize(new java.awt.Dimension(25,25));
      jButton137.setMinimumSize(new java.awt.Dimension(25,25));
      jButton137.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 19;
      jButton137.setText("166");
      top.add(jButton137, gridBagConstraints);

      jButton138.setMaximumSize(new java.awt.Dimension(25,25));
      jButton138.setMinimumSize(new java.awt.Dimension(25,25));
      jButton138.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 16;
      jButton138.setText("131");
      top.add(jButton138, gridBagConstraints);

      jButton139.setMaximumSize(new java.awt.Dimension(25,25));
      jButton139.setMinimumSize(new java.awt.Dimension(25,25));
      jButton139.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 12;
      jButton139.setText("94");
      top.add(jButton139, gridBagConstraints);

      jButton140.setMaximumSize(new java.awt.Dimension(25,25));
      jButton140.setMinimumSize(new java.awt.Dimension(25,25));
      jButton140.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 18;
      jButton140.setText("157");
      top.add(jButton140, gridBagConstraints);

      jButton141.setMaximumSize(new java.awt.Dimension(25,25));
      jButton141.setMinimumSize(new java.awt.Dimension(25,25));
      jButton141.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 11;
      gridBagConstraints.gridy = 15;
      jButton141.setText("114");
      top.add(jButton141, gridBagConstraints);

      jButton142.setMaximumSize(new java.awt.Dimension(25,25));
      jButton142.setMinimumSize(new java.awt.Dimension(25,25));
      jButton142.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 22;
      jButton142.setText("177");
      top.add(jButton142, gridBagConstraints);

      jButton143.setMaximumSize(new java.awt.Dimension(25,25));
      jButton143.setMinimumSize(new java.awt.Dimension(25,25));
      jButton143.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 5;
      jButton143.setText("28");
      top.add(jButton143, gridBagConstraints);

      jButton144.setMaximumSize(new java.awt.Dimension(25,25));
      jButton144.setMinimumSize(new java.awt.Dimension(25,25));
      jButton144.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 20;
      gridBagConstraints.gridy = 8;
      jButton144.setText("72");
      top.add(jButton144, gridBagConstraints);

      jButton145.setMaximumSize(new java.awt.Dimension(25,25));
      jButton145.setMinimumSize(new java.awt.Dimension(25,25));
      jButton145.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 6;
      jButton145.setText("37");
      top.add(jButton145, gridBagConstraints);

      jButton146.setMaximumSize(new java.awt.Dimension(25,25));
      jButton146.setMinimumSize(new java.awt.Dimension(25,25));
      jButton146.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 12;
      gridBagConstraints.gridy = 7;
      jButton146.setText("52");
      top.add(jButton146, gridBagConstraints);

      jButton149.setMaximumSize(new java.awt.Dimension(25,25));
      jButton149.setMinimumSize(new java.awt.Dimension(25,25));
      jButton149.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 20;
      gridBagConstraints.gridy = 6;
      jButton149.setText("44");
      top.add(jButton149, gridBagConstraints);

      jButton151.setMaximumSize(new java.awt.Dimension(25,25));
      jButton151.setMinimumSize(new java.awt.Dimension(25,25));
      jButton151.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 11;
      top.add(jButton151, gridBagConstraints);

      jButton152.setMaximumSize(new java.awt.Dimension(25,25));
      jButton152.setMinimumSize(new java.awt.Dimension(25,25));
      jButton152.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 4;
      jButton152.setText("22");
      top.add(jButton152, gridBagConstraints);

      jButton153.setMaximumSize(new java.awt.Dimension(25,25));
      jButton153.setMinimumSize(new java.awt.Dimension(25,25));
      jButton153.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 7;
      jButton153.setText("54");
      top.add(jButton153, gridBagConstraints);

      jButton154.setMaximumSize(new java.awt.Dimension(25,25));
      jButton154.setMinimumSize(new java.awt.Dimension(25,25));
      jButton154.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 9;
      jButton154.setText("77");
      top.add(jButton154, gridBagConstraints);

      

      jButton156.setMaximumSize(new java.awt.Dimension(25,25));
      jButton156.setMinimumSize(new java.awt.Dimension(25,25));
      jButton156.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 5;
      jButton156.setText("33");
      top.add(jButton156, gridBagConstraints);

      jButton157.setMaximumSize(new java.awt.Dimension(25,25));
      jButton157.setMinimumSize(new java.awt.Dimension(25,25));
      jButton157.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 5;
      jButton157.setText("35");
      top.add(jButton157, gridBagConstraints);

      jButton160.setMaximumSize(new java.awt.Dimension(25,25));
      jButton160.setMinimumSize(new java.awt.Dimension(25,25));
      jButton160.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 5;
      top.add(jButton160, gridBagConstraints);

      
      jButton164.setMaximumSize(new java.awt.Dimension(25,25));
      jButton164.setMinimumSize(new java.awt.Dimension(25,25));
      jButton164.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 5;
      jButton164.setText("29");
      top.add(jButton164, gridBagConstraints);

      jButton165.setMaximumSize(new java.awt.Dimension(25,25));
      jButton165.setMinimumSize(new java.awt.Dimension(25,25));
      jButton165.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 5;
      top.add(jButton165, gridBagConstraints);

      jButton166.setMaximumSize(new java.awt.Dimension(25,25));
      jButton166.setMinimumSize(new java.awt.Dimension(25,25));
      jButton166.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 5;
      
      top.add(jButton166, gridBagConstraints);

      jButton167.setBackground(new java.awt.Color(0, 0, 0));
      jButton167.setForeground(Color.WHITE);
      jButton167.setMaximumSize(new java.awt.Dimension(25,25));
      jButton167.setMinimumSize(new java.awt.Dimension(25,25));
      jButton167.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 9;
      gridBagConstraints.gridy = 4;
      jButton167.setText("197");
      top.add(jButton167, gridBagConstraints);

      

      jButton170.setMaximumSize(new java.awt.Dimension(25,25));
      jButton170.setMinimumSize(new java.awt.Dimension(25,25));
      jButton170.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 3;
      jButton170.setText("13");
      top.add(jButton170, gridBagConstraints);

      jButton174.setMaximumSize(new java.awt.Dimension(25,25));
      jButton174.setMinimumSize(new java.awt.Dimension(25,25));
      jButton174.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 2;
      jButton174.setText("10");
      top.add(jButton174, gridBagConstraints);

      jButton175.setMaximumSize(new java.awt.Dimension(25,25));
      jButton175.setMinimumSize(new java.awt.Dimension(25,25));
      jButton175.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 4;
      jButton175.setText("24");
      top.add(jButton175, gridBagConstraints);

      jButton176.setMaximumSize(new java.awt.Dimension(25,25));
      jButton176.setMinimumSize(new java.awt.Dimension(25,25));
      jButton176.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 6;
      jButton176.setText("40");
      top.add(jButton176, gridBagConstraints);

      jButton177.setMaximumSize(new java.awt.Dimension(25,25));
      jButton177.setMinimumSize(new java.awt.Dimension(25,25));
      jButton177.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 5;
      top.add(jButton177, gridBagConstraints);

      jButton178.setBackground(Color.BLACK);
      jButton178.setForeground(Color.WHITE);
      jButton178.setMaximumSize(new java.awt.Dimension(25,25));
      jButton178.setMinimumSize(new java.awt.Dimension(25,25));
      jButton178.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 9;
      gridBagConstraints.gridy = 17;
      jButton178.setText("192");
      top.add(jButton178, gridBagConstraints);

      jButton179.setMaximumSize(new java.awt.Dimension(25,25));
      jButton179.setMinimumSize(new java.awt.Dimension(25,25));
      jButton179.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 5;
      top.add(jButton179, gridBagConstraints);

      jButton180.setMaximumSize(new java.awt.Dimension(25,25));
      jButton180.setMinimumSize(new java.awt.Dimension(25,25));
      jButton180.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 1;
      jButton180.setText("6");
      top.add(jButton180, gridBagConstraints);

      jButton181.setMaximumSize(new java.awt.Dimension(25,25));
      jButton181.setMinimumSize(new java.awt.Dimension(25,25));
      jButton181.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 5;
      top.add(jButton181, gridBagConstraints);

      jButton182.setBackground(new java.awt.Color(0, 0, 0));
      jButton182.setForeground(Color.WHITE);
      jButton182.setMaximumSize(new java.awt.Dimension(25,25));
      jButton182.setMinimumSize(new java.awt.Dimension(25,25));
      jButton182.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 9;
      jButton182.setText("190");
      top.add(jButton182, gridBagConstraints);

      jButton183.setMaximumSize(new java.awt.Dimension(25,25));
      jButton183.setMinimumSize(new java.awt.Dimension(25,25));
      jButton183.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 4;
      jButton183.setText("16");
      top.add(jButton183, gridBagConstraints);

      jButton184.setMaximumSize(new java.awt.Dimension(25,25));
      jButton184.setMinimumSize(new java.awt.Dimension(25,25));
      jButton184.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 5;
      top.add(jButton184, gridBagConstraints);

      jButton185.setMaximumSize(new java.awt.Dimension(25,25));
      jButton185.setMinimumSize(new java.awt.Dimension(25,25));
      jButton185.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 9;
      jButton185.setText("76");
      top.add(jButton185, gridBagConstraints);

     
      jButton188.setMaximumSize(new java.awt.Dimension(25,25));
      jButton188.setMinimumSize(new java.awt.Dimension(25,25));
      jButton188.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 10;
      gridBagConstraints.gridy = 7;
      jButton188.setText("50");
      top.add(jButton188, gridBagConstraints);

      jButton189.setMaximumSize(new java.awt.Dimension(25,25));
      jButton189.setMinimumSize(new java.awt.Dimension(25,25));
      jButton189.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 11;
      jButton189.setText("86");
      top.add(jButton189, gridBagConstraints);

      jButton190.setBackground(new java.awt.Color(0, 0, 0));
      jButton190.setForeground(Color.WHITE);
      jButton190.setMaximumSize(new java.awt.Dimension(25,25));
      jButton190.setMinimumSize(new java.awt.Dimension(25,25));
      jButton190.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 5;
      jButton190.setText("189");
      top.add(jButton190, gridBagConstraints);


      jButton193.setMaximumSize(new java.awt.Dimension(25,25));
      jButton193.setMinimumSize(new java.awt.Dimension(25,25));
      jButton193.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 11;
      top.add(jButton193, gridBagConstraints);

      jButton201.setMaximumSize(new java.awt.Dimension(25,25));
      jButton201.setMinimumSize(new java.awt.Dimension(25,25));
      jButton201.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 6;
      jButton201.setText("36");
      top.add(jButton201, gridBagConstraints);

    

      jButton207.setMaximumSize(new java.awt.Dimension(25,25));
      jButton207.setMinimumSize(new java.awt.Dimension(25,25));
      jButton207.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 10;
      jButton207.setText("80");
      top.add(jButton207, gridBagConstraints);

      jButton208.setMaximumSize(new java.awt.Dimension(25,25));
      jButton208.setMinimumSize(new java.awt.Dimension(25,25));
      jButton208.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 5;
      top.add(jButton208, gridBagConstraints);

      jButton209.setMaximumSize(new java.awt.Dimension(25,25));
      jButton209.setMinimumSize(new java.awt.Dimension(25,25));
      jButton209.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 8;
      top.add(jButton209, gridBagConstraints);

      jButton210.setMaximumSize(new java.awt.Dimension(25,25));
      jButton210.setMinimumSize(new java.awt.Dimension(25,25));
      jButton210.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 5;
      top.add(jButton210, gridBagConstraints);

      jButton211.setMaximumSize(new java.awt.Dimension(25,25));
      jButton211.setMinimumSize(new java.awt.Dimension(25,25));
      jButton211.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 7;
      jButton211.setText("47");
      top.add(jButton211, gridBagConstraints);

      jButton212.setMaximumSize(new java.awt.Dimension(25,25));
      jButton212.setMinimumSize(new java.awt.Dimension(25,25));
      jButton212.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 4;
      jButton212.setText("18");
      top.add(jButton212, gridBagConstraints);

      jButton213.setMaximumSize(new java.awt.Dimension(25,25));
      jButton213.setMinimumSize(new java.awt.Dimension(25,25));
      jButton213.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 7;
      jButton213.setText("47");
      top.add(jButton213, gridBagConstraints);

      jButton214.setMaximumSize(new java.awt.Dimension(25,25));
      jButton214.setMinimumSize(new java.awt.Dimension(25,25));
      jButton214.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 11;
      top.add(jButton214, gridBagConstraints);

      jButton215.setBackground(new java.awt.Color(0, 0, 0));
      jButton215.setForeground(Color.WHITE);
      jButton215.setMaximumSize(new java.awt.Dimension(25,25));
      jButton215.setMinimumSize(new java.awt.Dimension(25,25));
      jButton215.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 10;
      jButton215.setText("195");
      top.add(jButton215, gridBagConstraints);

      

      jButton218.setMaximumSize(new java.awt.Dimension(25,25));
      jButton218.setMinimumSize(new java.awt.Dimension(25,25));
      jButton218.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 6;
      jButton218.setText("38");
      top.add(jButton218, gridBagConstraints);

      jButton219.setMaximumSize(new java.awt.Dimension(25,25));
      jButton219.setMinimumSize(new java.awt.Dimension(25,25));
      jButton219.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 6;
      top.add(jButton219, gridBagConstraints);

      jButton221.setMaximumSize(new java.awt.Dimension(25,25));
      jButton221.setMinimumSize(new java.awt.Dimension(25,25));
      jButton221.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 5;
      top.add(jButton221, gridBagConstraints);

      jButton222.setMaximumSize(new java.awt.Dimension(25,25));
      jButton222.setMinimumSize(new java.awt.Dimension(25,25));
      jButton222.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 5;
      top.add(jButton222, gridBagConstraints);

      jButton223.setMaximumSize(new java.awt.Dimension(25,25));
      jButton223.setMinimumSize(new java.awt.Dimension(25,25));
      jButton223.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 0;
      gridBagConstraints.gridy = 5;
      top.add(jButton223, gridBagConstraints);

      

      jButton225.setMaximumSize(new java.awt.Dimension(25,25));
      jButton225.setMinimumSize(new java.awt.Dimension(25,25));
      jButton225.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 5;
      top.add(jButton225, gridBagConstraints);

      
      jButton229.setMaximumSize(new java.awt.Dimension(25,25));
      jButton229.setMinimumSize(new java.awt.Dimension(25,25));
      jButton229.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 5;
      top.add(jButton229, gridBagConstraints);

      jButton230.setMaximumSize(new java.awt.Dimension(25,25));
      jButton230.setMinimumSize(new java.awt.Dimension(25,25));
      jButton230.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 5;
      top.add(jButton230, gridBagConstraints);

      jButton231.setMaximumSize(new java.awt.Dimension(25,25));
      jButton231.setMinimumSize(new java.awt.Dimension(25,25));
      jButton231.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 5;
      top.add(jButton231, gridBagConstraints);

      jButton232.setBackground(new java.awt.Color(0, 0, 0));
      jButton232.setForeground(Color.WHITE);
      jButton232.setMaximumSize(new java.awt.Dimension(25,25));
      jButton232.setMinimumSize(new java.awt.Dimension(25,25));
      jButton232.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 19;
      jButton232.setText("192");
      top.add(jButton232, gridBagConstraints);

      jButton233.setMaximumSize(new java.awt.Dimension(25,25));
      jButton233.setMinimumSize(new java.awt.Dimension(25,25));
      jButton233.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 5;
      top.add(jButton233, gridBagConstraints);

      jButton234.setMaximumSize(new java.awt.Dimension(25,25));
      jButton234.setMinimumSize(new java.awt.Dimension(25,25));
      jButton234.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 5;
      top.add(jButton234, gridBagConstraints);

      jButton235.setMaximumSize(new java.awt.Dimension(25,25));
      jButton235.setMinimumSize(new java.awt.Dimension(25,25));
      jButton235.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 4;
      jButton235.setText("21");
      top.add(jButton235, gridBagConstraints);

      jButton236.setMaximumSize(new java.awt.Dimension(25,25));
      jButton236.setMinimumSize(new java.awt.Dimension(25,25));
      jButton236.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 0;
      gridBagConstraints.gridy = 5;
      top.add(jButton236, gridBagConstraints);


      jButton238.setMaximumSize(new java.awt.Dimension(25,25));
      jButton238.setMinimumSize(new java.awt.Dimension(25,25));
      jButton238.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 5;
      top.add(jButton238, gridBagConstraints);

      jButton240.setMaximumSize(new java.awt.Dimension(25,25));
      jButton240.setMinimumSize(new java.awt.Dimension(25,25));
      jButton240.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 18;
      gridBagConstraints.gridy = 17;
      jButton240.setText("148");
      top.add(jButton240, gridBagConstraints);

      jButton241.setMaximumSize(new java.awt.Dimension(25,25));
      jButton241.setMinimumSize(new java.awt.Dimension(25,25));
      jButton241.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 3;
      gridBagConstraints.gridy = 4;
      top.add(jButton241, gridBagConstraints);

      jButton242.setMaximumSize(new java.awt.Dimension(25,25));
      jButton242.setMinimumSize(new java.awt.Dimension(25,25));
      jButton242.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 5;
      top.add(jButton242, gridBagConstraints);

      jButton243.setMaximumSize(new java.awt.Dimension(25,25));
      jButton243.setMinimumSize(new java.awt.Dimension(25,25));
      jButton243.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 5;
      top.add(jButton243, gridBagConstraints);

      jButton244.setMaximumSize(new java.awt.Dimension(25,25));
      jButton244.setMinimumSize(new java.awt.Dimension(25,25));
      jButton244.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 5;
      top.add(jButton244, gridBagConstraints);

      jButton245.setMaximumSize(new java.awt.Dimension(25,25));
      jButton245.setMinimumSize(new java.awt.Dimension(25,25));
      jButton245.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 4;
      jButton245.setText("19");
      top.add(jButton245, gridBagConstraints);

      jButton246.setMaximumSize(new java.awt.Dimension(25,25));
      jButton246.setMinimumSize(new java.awt.Dimension(25,25));
      jButton246.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 5;
      top.add(jButton246, gridBagConstraints);

      jButton247.setBackground(java.awt.Color.black);
      jButton247.setForeground(Color.WHITE);
      jButton247.setMaximumSize(new java.awt.Dimension(25,25));
      jButton247.setMinimumSize(new java.awt.Dimension(25,25));
      jButton247.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 8;
      jButton247.setText("195");
      top.add(jButton247, gridBagConstraints);

      jButton248.setMaximumSize(new java.awt.Dimension(25,25));
      jButton248.setMinimumSize(new java.awt.Dimension(25,25));
      jButton248.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 11;
      jButton248.setText("92");
      top.add(jButton248, gridBagConstraints);

      jButton249.setMaximumSize(new java.awt.Dimension(25,25));
      jButton249.setMinimumSize(new java.awt.Dimension(25,25));
      jButton249.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 5;
      top.add(jButton249, gridBagConstraints);

  

      jButton252.setMaximumSize(new java.awt.Dimension(25,25));
      jButton252.setMinimumSize(new java.awt.Dimension(25,25));
      jButton252.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 11;
      top.add(jButton252, gridBagConstraints);

      jButton253.setMaximumSize(new java.awt.Dimension(25,25));
      jButton253.setMinimumSize(new java.awt.Dimension(25,25));
      jButton253.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 11;
      gridBagConstraints.gridy = 7;
      jButton253.setText("40");
      top.add(jButton253, gridBagConstraints);

    
      jButton255.setBackground(new java.awt.Color(0, 0, 0));
      jButton255.setForeground(Color.WHITE);
      jButton255.setMaximumSize(new java.awt.Dimension(25,25));
      jButton255.setMinimumSize(new java.awt.Dimension(25,25));
      jButton255.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 12;
      gridBagConstraints.gridy = 6;
      jButton255.setText("197");
      top.add(jButton255, gridBagConstraints);

      jButton256.setMaximumSize(new java.awt.Dimension(25,25));
      jButton256.setMinimumSize(new java.awt.Dimension(25,25));
      jButton256.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 11;
      top.add(jButton256, gridBagConstraints);

          
      jButton260.setMaximumSize(new java.awt.Dimension(25,25));
      jButton260.setMinimumSize(new java.awt.Dimension(25,25));
      jButton260.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 11;
      top.add(jButton260, gridBagConstraints);

      jButton266.setMaximumSize(new java.awt.Dimension(25,25));
      jButton266.setMinimumSize(new java.awt.Dimension(25,25));
      jButton266.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 10;
      jButton266.setText("79");
      top.add(jButton266, gridBagConstraints);

      jButton273.setMaximumSize(new java.awt.Dimension(25,25));
      jButton273.setMinimumSize(new java.awt.Dimension(25,25));
      jButton273.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 18;
      jButton273.setText("162");
      top.add(jButton273, gridBagConstraints);

      jButton274.setMaximumSize(new java.awt.Dimension(25,25));
      jButton274.setMinimumSize(new java.awt.Dimension(25,25));
      jButton274.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 17;
      jButton274.setText("145");
      top.add(jButton274, gridBagConstraints);

     
      jButton276.setMaximumSize(new java.awt.Dimension(25,25));
      jButton276.setMinimumSize(new java.awt.Dimension(25,25));
      jButton276.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 19;
      gridBagConstraints.gridy = 7;
      jButton276.setText("59");
      top.add(jButton276, gridBagConstraints);

      jButton277.setMaximumSize(new java.awt.Dimension(25,25));
      jButton277.setMinimumSize(new java.awt.Dimension(25,25));
      jButton277.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 21;
      jButton277.setText("176");
      top.add(jButton277, gridBagConstraints);

      

      jButton279.setMaximumSize(new java.awt.Dimension(25,25));
      jButton279.setMinimumSize(new java.awt.Dimension(25,25));
      jButton279.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 8;
      jButton279.setText("69");
      top.add(jButton279, gridBagConstraints);

      jButton280.setMaximumSize(new java.awt.Dimension(25,25));
      jButton280.setMinimumSize(new java.awt.Dimension(25,25));
      jButton280.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 19;
      gridBagConstraints.gridy = 8;
      jButton280.setText("71");
      top.add(jButton280, gridBagConstraints);

      jButton281.setMaximumSize(new java.awt.Dimension(25,25));
      jButton281.setMinimumSize(new java.awt.Dimension(25,25));
      jButton281.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 16;
      jButton281.setText("123");
      top.add(jButton281, gridBagConstraints);

      jButton282.setMaximumSize(new java.awt.Dimension(25,25));
      jButton282.setMinimumSize(new java.awt.Dimension(25,25));
      jButton282.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 23;
      jButton282.setText("184");
      top.add(jButton282, gridBagConstraints);

      jButton284.setMaximumSize(new java.awt.Dimension(25,25));
      jButton284.setMinimumSize(new java.awt.Dimension(25,25));
      jButton284.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 15;
      jButton284.setText("111");
      top.add(jButton284, gridBagConstraints);



      jButton286.setMaximumSize(new java.awt.Dimension(25,25));
      jButton286.setMinimumSize(new java.awt.Dimension(25,25));
      jButton286.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 17;
      jButton286.setText("40");
      top.add(jButton286, gridBagConstraints);

      jButton287.setBackground(new java.awt.Color(0, 0, 0));
      jButton287.setForeground(Color.WHITE);
      jButton287.setMaximumSize(new java.awt.Dimension(25,25));
      jButton287.setMinimumSize(new java.awt.Dimension(25,25));
      jButton287.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 19;
      jButton287.setText("192");
      top.add(jButton287, gridBagConstraints);

      jButton289.setMaximumSize(new java.awt.Dimension(25,25));
      jButton289.setMinimumSize(new java.awt.Dimension(25,25));
      jButton289.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 18;
      gridBagConstraints.gridy = 7;
      jButton289.setText("58");
      top.add(jButton289, gridBagConstraints);

      jButton290.setMaximumSize(new java.awt.Dimension(25,25));
      jButton290.setMinimumSize(new java.awt.Dimension(25,25));
      jButton290.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 20;
      jButton290.setText("172");
      top.add(jButton290, gridBagConstraints);


      jButton293.setMaximumSize(new java.awt.Dimension(25,25));
      jButton293.setMinimumSize(new java.awt.Dimension(25,25));
      jButton293.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 12;
      jButton293.setText("95");
      top.add(jButton293, gridBagConstraints);

      jButton294.setMaximumSize(new java.awt.Dimension(25,25));
      jButton294.setMinimumSize(new java.awt.Dimension(25,25));
      jButton294.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 10;
      gridBagConstraints.gridy = 16;
      jButton294.setText("126");
      top.add(jButton294, gridBagConstraints);

      jButton295.setMaximumSize(new java.awt.Dimension(25,25));
      jButton295.setMinimumSize(new java.awt.Dimension(25,25));
      jButton295.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 15;
      jButton295.setText("117");
      top.add(jButton295, gridBagConstraints);

      jButton296.setBackground(new java.awt.Color(0, 0, 0));
      jButton296.setForeground(Color.WHITE);
      jButton296.setMaximumSize(new java.awt.Dimension(25,25));
      jButton296.setMinimumSize(new java.awt.Dimension(25,25));
      jButton296.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 12;
      jButton296.setText("194");
      top.add(jButton296, gridBagConstraints);

      jButton298.setMaximumSize(new java.awt.Dimension(25,25));
      jButton298.setMinimumSize(new java.awt.Dimension(25,25));
      jButton298.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 20;
      jButton298.setText("171");
      top.add(jButton298, gridBagConstraints);

      jButton299.setMaximumSize(new java.awt.Dimension(25,25));
      jButton299.setMinimumSize(new java.awt.Dimension(25,25));
      jButton299.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 22;
      gridBagConstraints.gridy = 8;
      jButton299.setText("74");
      top.add(jButton299, gridBagConstraints);

     
      jButton301.setMaximumSize(new java.awt.Dimension(25,25));
      jButton301.setMinimumSize(new java.awt.Dimension(25,25));
      jButton301.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 24;
      jButton301.setText("188");
      top.add(jButton301, gridBagConstraints);

      jButton302.setBackground(new java.awt.Color(0, 0, 0));
      jButton302.setForeground(Color.WHITE);
      jButton302.setMaximumSize(new java.awt.Dimension(25,25));
      jButton302.setMinimumSize(new java.awt.Dimension(25,25));
      jButton302.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 11;
      gridBagConstraints.gridy = 6;
      jButton302.setText("197");
      top.add(jButton302, gridBagConstraints);

      
      jButton305.setMaximumSize(new java.awt.Dimension(25,25));
      jButton305.setMinimumSize(new java.awt.Dimension(25,25));
      jButton305.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 7;
      jButton305.setText("57");
      top.add(jButton305, gridBagConstraints);

      
      jButton309.setBackground(new java.awt.Color(0, 0, 0));
      jButton309.setForeground(Color.WHITE);
      jButton309.setMaximumSize(new java.awt.Dimension(25,25));
      jButton309.setMinimumSize(new java.awt.Dimension(25,25));
      jButton309.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 4;
      gridBagConstraints.gridy = 19;
      jButton309.setText("193");
      top.add(jButton309, gridBagConstraints);

      jButton311.setMaximumSize(new java.awt.Dimension(25,25));
      jButton311.setMinimumSize(new java.awt.Dimension(25,25));
      jButton311.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 10;
      gridBagConstraints.gridy = 15;
      jButton311.setText("113");
      top.add(jButton311, gridBagConstraints);

      jButton312.setBackground(new java.awt.Color(0, 0, 0));
      jButton312.setMaximumSize(new java.awt.Dimension(25,25));
      jButton312.setMinimumSize(new java.awt.Dimension(25,25));
      jButton312.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 19;
      gridBagConstraints.gridy = 18;
      jButton312.setText("191");
      top.add(jButton312, gridBagConstraints);

      

      jButton314.setMaximumSize(new java.awt.Dimension(25,25));
      jButton314.setMinimumSize(new java.awt.Dimension(25,25));
      jButton314.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 11;
      top.add(jButton314, gridBagConstraints);

      jButton315.setBackground(new java.awt.Color(0, 0, 0));
      jButton315.setForeground(Color.WHITE);
      jButton315.setMaximumSize(new java.awt.Dimension(25,25));
      jButton315.setMinimumSize(new java.awt.Dimension(25,25));
      jButton315.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 3;
      jButton315.setText("196");
      top.add(jButton315, gridBagConstraints);

    

      jButton332.setBackground(new java.awt.Color(0, 0, 0));
      jButton332.setForeground(Color.WHITE);
      jButton332.setMaximumSize(new java.awt.Dimension(25,25));
      jButton332.setMinimumSize(new java.awt.Dimension(25,25));
      jButton332.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 5;
      gridBagConstraints.gridy = 15;
      jButton332.setText("194");
      top.add(jButton332, gridBagConstraints);

     
      jButton335.setMaximumSize(new java.awt.Dimension(25,25));
      jButton335.setMinimumSize(new java.awt.Dimension(25,25));
      jButton335.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 8;
      gridBagConstraints.gridy = 13;
      jButton335.setText("101");
      top.add(jButton335, gridBagConstraints);

      
      jButton338.setMaximumSize(new java.awt.Dimension(25,25));
      jButton338.setMinimumSize(new java.awt.Dimension(25,25));
      jButton338.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 18;
      gridBagConstraints.gridy = 8;
      jButton338.setText("70");
      top.add(jButton338, gridBagConstraints);

      jButton339.setMaximumSize(new java.awt.Dimension(25,25));
      jButton339.setMinimumSize(new java.awt.Dimension(25,25));
      jButton339.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 20;
      gridBagConstraints.gridy = 8;
      top.add(jButton339, gridBagConstraints);

      jButton340.setMaximumSize(new java.awt.Dimension(25,25));
      jButton340.setMinimumSize(new java.awt.Dimension(25,25));
      jButton340.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 9;
      gridBagConstraints.gridy = 16;
      jButton340.setText("125");
      top.add(jButton340, gridBagConstraints);

      jButton341.setBackground(new java.awt.Color(0, 0, 0));
      jButton341.setMaximumSize(new java.awt.Dimension(25,25));
      jButton341.setMinimumSize(new java.awt.Dimension(25,25));
      jButton341.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 12;
      jButton341.setText("190");
      top.add(jButton341, gridBagConstraints);

      jButton343.setMaximumSize(new java.awt.Dimension(25,25));
      jButton343.setMinimumSize(new java.awt.Dimension(25,25));
      jButton343.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 19;
      jButton343.setText("167");
      top.add(jButton343, gridBagConstraints);

     

      jButton348.setMaximumSize(new java.awt.Dimension(25,25));
      jButton348.setMinimumSize(new java.awt.Dimension(25,25));
      jButton348.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 22;
      jButton348.setText("179");
      top.add(jButton348, gridBagConstraints);

     
      jButton352.setMaximumSize(new java.awt.Dimension(25,25));
      jButton352.setMinimumSize(new java.awt.Dimension(25,25));
      jButton352.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 21;
      gridBagConstraints.gridy = 8;
      jButton352.setText("73");
      top.add(jButton352, gridBagConstraints);

      jButton353.setMaximumSize(new java.awt.Dimension(25,25));
      jButton353.setMinimumSize(new java.awt.Dimension(25,25));
      jButton353.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 11;
      gridBagConstraints.gridy = 16;
      jButton353.setText("127");
      top.add(jButton353, gridBagConstraints);

      jButton354.setMaximumSize(new java.awt.Dimension(25,25));
      jButton354.setMinimumSize(new java.awt.Dimension(25,25));
      jButton354.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 15;
      jButton354.setText("118");
      top.add(jButton354, gridBagConstraints);

      jButton357.setMaximumSize(new java.awt.Dimension(25,25));
      jButton357.setMinimumSize(new java.awt.Dimension(25,25));
      jButton357.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 21;
      jButton357.setText("175");
      top.add(jButton357, gridBagConstraints);

      jButton358.setMaximumSize(new java.awt.Dimension(25,25));
      jButton358.setMinimumSize(new java.awt.Dimension(25,25));
      jButton358.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 14;
      jButton358.setText("105");
      top.add(jButton358, gridBagConstraints);

    
      jButton360.setMaximumSize(new java.awt.Dimension(25,25));
      jButton360.setMinimumSize(new java.awt.Dimension(25,25));
      jButton360.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 15;
      jButton360.setText("109");
      top.add(jButton360, gridBagConstraints);

     
      jButton364.setMaximumSize(new java.awt.Dimension(25,25));
      jButton364.setMinimumSize(new java.awt.Dimension(25,25));
      jButton364.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 16;
      gridBagConstraints.gridy = 23;
      jButton364.setText("186");
      top.add(jButton364, gridBagConstraints);

      jButton365.setMaximumSize(new java.awt.Dimension(25,25));
      jButton365.setMinimumSize(new java.awt.Dimension(25,25));
      jButton365.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 14;
      jButton365.setText("104");
      top.add(jButton365, gridBagConstraints);

     
      jButton369.setMaximumSize(new java.awt.Dimension(25,25));
      jButton369.setMinimumSize(new java.awt.Dimension(25,25));
      jButton369.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 6;
      gridBagConstraints.gridy = 16;
      jButton369.setText("122");
      top.add(jButton369, gridBagConstraints);

      jButton370.setMaximumSize(new java.awt.Dimension(25,25));
      jButton370.setMinimumSize(new java.awt.Dimension(25,25));
      jButton370.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 9;
      gridBagConstraints.gridy = 15;
      jButton370.setText("112");
      top.add(jButton370, gridBagConstraints);

  
      jButton380.setMaximumSize(new java.awt.Dimension(25,25));
      jButton380.setMinimumSize(new java.awt.Dimension(25,25));
      jButton380.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 7;
      gridBagConstraints.gridy = 10;
      top.add(jButton380, gridBagConstraints);

    

      jButton392.setMaximumSize(new java.awt.Dimension(25,25));
      jButton392.setMinimumSize(new java.awt.Dimension(25,25));
      jButton392.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 12;
      jButton392.setText("98");
      top.add(jButton392, gridBagConstraints);

      jButton393.setMaximumSize(new java.awt.Dimension(25,25));
      jButton393.setMinimumSize(new java.awt.Dimension(25,25));
      jButton393.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 21;
      gridBagConstraints.gridy = 17;
      jButton393.setText("151");
      top.add(jButton393, gridBagConstraints);

      jButton394.setMaximumSize(new java.awt.Dimension(25,25));
      jButton394.setMinimumSize(new java.awt.Dimension(25,25));
      jButton394.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 18;
      gridBagConstraints.gridy = 16;
      jButton394.setText("134");
      top.add(jButton394, gridBagConstraints);

      jButton395.setMaximumSize(new java.awt.Dimension(25,25));
      jButton395.setMinimumSize(new java.awt.Dimension(25,25));
      jButton395.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 15;
      jButton395.setText("120");
      top.add(jButton395, gridBagConstraints);

      jButton396.setMaximumSize(new java.awt.Dimension(25,25));
      jButton396.setMinimumSize(new java.awt.Dimension(25,25));
      jButton396.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 12;
      jButton396.setText("97");
      top.add(jButton396, gridBagConstraints);

      jButton397.setMaximumSize(new java.awt.Dimension(25,25));
      jButton397.setMinimumSize(new java.awt.Dimension(25,25));
      jButton397.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 19;
      gridBagConstraints.gridy = 16;
      jButton397.setText("135");
      top.add(jButton397, gridBagConstraints);

      jButton398.setMaximumSize(new java.awt.Dimension(25,25));
      jButton398.setMinimumSize(new java.awt.Dimension(25,25));
      jButton398.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 21;
      gridBagConstraints.gridy = 16;
      jButton398.setText("137");
      top.add(jButton398, gridBagConstraints);


      jButton400.setMaximumSize(new java.awt.Dimension(25,25));
      jButton400.setMinimumSize(new java.awt.Dimension(25,25));
      jButton400.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 19;
      jButton400.setText("168");
      top.add(jButton400, gridBagConstraints);

      jButton401.setMaximumSize(new java.awt.Dimension(25,25));
      jButton401.setMinimumSize(new java.awt.Dimension(25,25));
      jButton401.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 17;
      jButton401.setText("147");
      top.add(jButton401, gridBagConstraints);

      jButton402.setMaximumSize(new java.awt.Dimension(25,25));
      jButton402.setMinimumSize(new java.awt.Dimension(25,25));
      jButton402.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 19;
      gridBagConstraints.gridy = 17;
      jButton402.setText("149");
      top.add(jButton402, gridBagConstraints);

     

      jButton405.setMaximumSize(new java.awt.Dimension(25,25));
      jButton405.setMinimumSize(new java.awt.Dimension(25,25));
      jButton405.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 20;
      gridBagConstraints.gridy = 17;
      jButton405.setText("150");
      top.add(jButton405, gridBagConstraints);

      jButton406.setMaximumSize(new java.awt.Dimension(25,25));
      jButton406.setMinimumSize(new java.awt.Dimension(25,25));
      jButton406.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 16;
      jButton406.setText("133");
      top.add(jButton406, gridBagConstraints);

      jButton407.setMaximumSize(new java.awt.Dimension(25,25));
      jButton407.setMinimumSize(new java.awt.Dimension(25,25));
      jButton407.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 18;
      gridBagConstraints.gridy = 15;
      jButton407.setText("121");
      top.add(jButton407, gridBagConstraints);

      jButton409.setMaximumSize(new java.awt.Dimension(25,25));
      jButton409.setMinimumSize(new java.awt.Dimension(25,25));
      jButton409.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 20;
      gridBagConstraints.gridy = 16;
      jButton409.setText("136");
      top.add(jButton409, gridBagConstraints);

      jButton410.setMaximumSize(new java.awt.Dimension(25,25));
      jButton410.setMinimumSize(new java.awt.Dimension(25,25));
      jButton410.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 15;
      gridBagConstraints.gridy = 13;
      jButton410.setText("103");
      top.add(jButton410, gridBagConstraints);

      jButton413.setMaximumSize(new java.awt.Dimension(25,25));
      jButton413.setMinimumSize(new java.awt.Dimension(25,25));
      jButton413.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 17;
      gridBagConstraints.gridy = 18;
      jButton413.setText("163");
      top.add(jButton413, gridBagConstraints);

      jButton414.setMaximumSize(new java.awt.Dimension(25,25));
      jButton414.setMinimumSize(new java.awt.Dimension(25,25));
      jButton414.setPreferredSize(new java.awt.Dimension(25,25));
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 14;
      gridBagConstraints.gridy = 10;
      jButton414.setText("82");
      top.add(jButton414, gridBagConstraints);

      jLabel2.setText("Library");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 7;
      gridBagConstraints.gridwidth = 3;
      top.add(jLabel2, gridBagConstraints);

      jLabel3.setText("Study");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 2;
      gridBagConstraints.gridy = 1;
      gridBagConstraints.gridwidth = 4;
      top.add(jLabel3, gridBagConstraints);

      jLabel4.setText("Hall");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 10;
      gridBagConstraints.gridy = 2;
      gridBagConstraints.gridwidth = 4;
      top.add(jLabel4, gridBagConstraints);

      jLabel5.setText("Lounge");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 18;
      gridBagConstraints.gridy = 3;
      gridBagConstraints.gridwidth = 5;
      top.add(jLabel5, gridBagConstraints);

      jLabel6.setText("Dining Room");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 18;
      gridBagConstraints.gridy = 11;
      gridBagConstraints.gridwidth = 3;
      top.add(jLabel6, gridBagConstraints);

      jLabel7.setText("Billard Room");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 14;
      gridBagConstraints.gridwidth = 4;
      top.add(jLabel7, gridBagConstraints);

      jLabel8.setText("Ballroom");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 10;
      gridBagConstraints.gridy = 20;
      gridBagConstraints.gridwidth = 4;
      top.add(jLabel8, gridBagConstraints);

      jLabel9.setText("Conservatory");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 1;
      gridBagConstraints.gridy = 21;
      gridBagConstraints.gridwidth = 4;
      top.add(jLabel9, gridBagConstraints);

      jLabel10.setText("Kitchen");
      gridBagConstraints = new java.awt.GridBagConstraints();
      gridBagConstraints.gridx = 19;
      gridBagConstraints.gridy = 21;
      gridBagConstraints.gridwidth = 4;
      top.add(jLabel10, gridBagConstraints);
      jLabel1.setText("jLabel1");
      jLabel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

 
  pack();
  setSize(900,810);
  setVisible(true);
	}
	
	public void cardButtonMaker(Player player) throws IOException
	{
		
		for(int i = 0; i < player.cardsInHand.size(); i++)
		{
			String s = player.cardsInHand.get(i);
			card = new JButton();
			card.setText(s);
			botmid.add(card);
		}
		
		sug = new JButton("Make a Suggestion");
		sug.addActionListener(new SuggestionClickListener());
		botleft.add(sug, BorderLayout.NORTH);
		
		JButton acc = new JButton("Make an Accusation");
		acc.addActionListener(new AccusationClickListener());
		botleft.add(acc, BorderLayout.AFTER_LINE_ENDS);
		
		
		JButton end = new JButton("End Your Turn");
		end.addActionListener(new EndClickListener(end));
		botleft.add(end, BorderLayout.SOUTH);
		
		JButton Dice = new JButton();
		Dice.setText("Roll");
		Dice.addActionListener(new DieClickListener(Dice));
		botleft.add(Dice, BorderLayout.LINE_START);
		
		
	}
	
//	public void userPortrait() throws IOException
//	{
//		
//		BufferedImage image = null;
//		URL url;
//		String s = Cards.portaits.removeFirst();
//		try {
//			url = new URL(s);
//			image = ImageIO.read(url);
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		JLabel Character = new JLabel();
//		Character.setIcon(new ImageIcon(image));
//		botleft.add(Character, BorderLayout.LINE_END);
//		Cards.portaits.offer(s);
//	}
//	
    public class DieClickListener implements ActionListener 
    {
    	private JButton _b;
    	
    	public DieClickListener(JButton b) {
    		_b = b;
    	}

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			// TODO Auto-generated method stub
			String d = new String();
			while(d.isEmpty())
			{
			d = "" + Game.diceRoll();
			_b.setText(d);
			_b.setEnabled(false);
			}
		}
    }
    
    public class AccusationClickListener implements ActionListener
    {
    	
        	public AccusationClickListener() {
        	}
    		public void actionPerformed(ActionEvent e) 
    		{
    			// TODO Auto-generated method stub
    			new accusationGUI(Game.players.peekFirst());
    			GameBoardGUI.this.setVisible(false);
    			
    			
    			
    		}
    }
    
    public class EndClickListener implements ActionListener 
    {
    	private JButton _b;
    	
    	public EndClickListener(JButton b) {
    		_b = b;
    	}

		@SuppressWarnings("deprecation")
		@Override
		public void actionPerformed(ActionEvent e) 
		{
			
			Player p = Game.players.pollFirst();
			System.out.println(p._CharacterUsed + " ended their turn.");
			Game.players.offer(p);
			try {
				new GameBoardGUI(Game.players.peekFirst());
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			GameBoardGUI.this.setVisible(false);
			
		}
	}
    
    public class SuggestionClickListener implements ActionListener 
    {
    	public SuggestionClickListener() {
    	}
		public void actionPerformed(ActionEvent e) 
		{
			// TODO Auto-generated method stub
			new suggestionGUI(Game.players.peekFirst());
			GameBoardGUI.this.setVisible(false);
			
		}
    }
}
        