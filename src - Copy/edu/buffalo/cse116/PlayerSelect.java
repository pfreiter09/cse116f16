

	package edu.buffalo.cse116;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PlayerSelect extends javax.swing.JFrame {

	    
	   
		private static final long serialVersionUID = 1L;
	    private javax.swing.JButton jButton;
	    private javax.swing.JPanel jPanel1;
	    private javax.swing.JPanel top;
	    private JPanel window;
	    private int count;
	    private int numbPlayers;
	    private javax.swing.JLabel jLabel;
	    private Game game;

		public PlayerSelect(int players) {
			count = 0;
			numbPlayers = players;
	        initComponents();
	       
	    }
	 private void initComponents() {
		 	setLocation(600,400);
		 	window = new JPanel(new BorderLayout());
	        jPanel1 = new javax.swing.JPanel();
	        jLabel = new javax.swing.JLabel();
	        jButton = new javax.swing.JButton();
	        top = new javax.swing.JPanel();

			top.add(jLabel);
			jLabel.setText("Player " + (count+1) + " choose your character!");
	        for(int i = 0; i < 6; i++)
	        {
	        	String name = Cards.Chars.get(i);
	        	jButton = new javax.swing.JButton();
	        	jButton.setText(name);
		        jButton.addActionListener(new PlayerClickListener(name, jButton));
		        jPanel1.add(jButton);
		        
		        
	        }
	        window.add(jPanel1, BorderLayout.SOUTH);
	        window.add(top, BorderLayout.NORTH);
	        this.add(window);
	        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
	        
	        

	        pack();
	        setVisible(true);
	    }                       
    
	    public class PlayerClickListener implements ActionListener 
	    {
	    	
	    	private String _name;
	    	private JButton _b;
	    	
	    	public PlayerClickListener(String name, JButton b) {
	    		_b = b;
	    		_name = name;
	    	}

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				_b.setVisible(false);
				Game.players.add(new Player(_name));
				System.out.println("You have chosen: " + _name);
				count+=1;
				jLabel.setText("Player " + (count+1) + " choose your character!");
				if (count == numbPlayers) 
				{
					setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
					PlayerSelect.this.setVisible(false);
					Game.createPlayerHands(Game.players);
					try {
						new GameBoardGUI(Game.players.get(0));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				// Once count = numbPlayers, kill pane and start game
			}
	    }
	}
/*	        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
jPanel1.setLayout(jPanel1Layout);
jPanel1Layout.setHorizontalGroup(
    jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(jPanel1Layout.createSequentialGroup()
        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        .addContainerGap(28, Short.MAX_VALUE))
);
jPanel1Layout.setVerticalGroup(
    jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addGroup(jPanel1Layout.createSequentialGroup()
        .addGap(24, 24, 24)
        .addComponent(jLabel)
        .addGap(18, 18, 18)
        .addComponent(jButton)
        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
);

javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
getContentPane().setLayout(layout);
layout.setHorizontalGroup(
    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
);
layout.setVerticalGroup(
    layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
);
*/
