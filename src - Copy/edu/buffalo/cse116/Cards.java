package edu.buffalo.cse116;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Cards 
{

	public List<String> Suspects;
	public List<String> Weapons;
	public List<String> Rooms;
	public List<String> Center;
	public ArrayList<String> Deck;
	public static List<String> Chars;
		
	
	String[] Suspect = {"Miss Scarlett","Col. Mustard","Mrs. White","Rev. Green","Ms. Peacock","Prof. Plum"};
	String[] Weapon = {"Candlestick","Rope","Knife", "Lead Pipe","Pistol","Wrench"};
	String[] Room = {"Kitchen","Ballroom","Conservatory","Billiard Room","Library","Study","Hall","Lounge","Dining Room"};
	
	//Cards constructor
	public Cards()
	{
		Suspects = new ArrayList<String>(Arrays.asList(Suspect));  // Adds all the Strings from Array to ArrayList
		Weapons = new ArrayList<String>(Arrays.asList(Weapon));
		Rooms = new ArrayList<String>(Arrays.asList(Room));
		Chars = new ArrayList<String>(Arrays.asList(Suspect)); 
		Center = new ArrayList<String>();
		Deck = new ArrayList<String>();
		Deck.addAll(Suspects);
		Deck.addAll(Weapons);
		Deck.addAll(Rooms);
		
	}
	/**
	 * Creates the players for the game.
	 * @param numbPlayers the number of players who will be playing
	 * @return the list of players.
	 */
	public LinkedList<Player> createPlayers(int numbPlayers)
	{
		LinkedList<Player> players = new LinkedList<Player>();
		for(int i=0; i<numbPlayers;i++)
		{
			players.add(i, new Player(Suspect[i])); 
		}
		return players;
	}
	/**
	 * generates a random number.
	 * @param e
	 * @return
	 */
	public static int randInt(List<String> e) 
	{ // Method creates a random integer based on the size of the ArrayList

		Random rand = new Random();
		int shuffleNumber = rand.nextInt(e.size());
		return shuffleNumber;
	}
	/**
	 * shuffles the cards in the deck passed into the method.
	 * @param e
	 * @return
	 */
	public List<String> shuffleCards(List<String> e)
	{
		Collections.shuffle(e);
		return e;
	}
	/**
	 * creates the murder case by adding one card of each type to an accusation list.
	 * @param theDeck
	 * @return
	 */
	public List<String> centerCase(ArrayList<String> theDeck)
	{	
		Deck = theDeck;
		shuffleCards(Suspects);
		shuffleCards(Weapons);
		shuffleCards(Rooms);
		Center.add(Suspects.get(0));
		Center.add(Weapons.get(0));
		Center.add(Rooms.get(0));
		Deck.remove(Suspects.get(0));
		Deck.remove(Weapons.get(0));
		Deck.remove(Rooms.get(0));
		return Center;
	}
	//shuffles the deck in preparation to deal
	public List<String> shuffleToDeal()
	{
		shuffleCards(Deck);
		return Deck;
	}
	//Deals an individual card.
	public String dealCard(ArrayList<String> theDeck) 
	{
		Deck = theDeck;
		shuffleToDeal();
		return Deck.remove(0);
	}
	//Deals a full hand of cards to all players.
	public LinkedList<Player> playerDeal(LinkedList<Player> players, ArrayList<String> theDeck)
	{
		Deck = theDeck;
		while(Deck.isEmpty() == false)
		{
			for(int j = 0; j < players.size(); j++)
			{
				players.get(j).cardsInHand.add(dealCard(Deck));
				if(Deck.isEmpty() == true)
				{
					return players;
				}
			}
		}
		return players;
	}
	
	public List<String> getSuspects(){
		return Suspects;
	}
	public List<String> getWeapons(){
		return Weapons;
	}
	public List<String> getRooms(){
		return Rooms;
	}
}
	
