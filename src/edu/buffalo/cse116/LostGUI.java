package edu.buffalo.cse116;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
public class LostGUI {

		private JFrame _window;
		public LostGUI(){
			_window = new JFrame("That Stinks");
			JPanel win = new JPanel();
			JLabel congrats = new JLabel();
			congrats.setText("You lose.....darn");
			congrats.setOpaque(true);
			congrats.setFont(new Font("Monaco", Font.BOLD, 44));
			win.setBackground(Color.WHITE);
			win.add(congrats);
			
			JButton okay = new JButton("okay");
			okay.addActionListener(new ActionListener(){
			
			public void actionPerformed(ActionEvent e) {
					_window.setVisible(false);
			}
			});
			
			win.add(okay);
			_window.add(win);
			_window.pack();
			_window.setVisible(true);
			_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		
}



