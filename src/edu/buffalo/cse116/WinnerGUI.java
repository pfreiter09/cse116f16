package edu.buffalo.cse116;

import java.awt.Color;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class WinnerGUI {
	private JFrame _window;
	public WinnerGUI(){
		_window = new JFrame("YAY YOU WIN");
		JPanel win = new JPanel();
		JLabel congrats = new JLabel();
		congrats.setText("YOU WERE RIGHT GO YOU");
		congrats.setOpaque(true);
		congrats.setFont(new Font("Monaco", Font.BOLD, 44));
		win.setBackground(Color.WHITE);
		win.add(congrats);
		_window.add(win);
		_window.pack();
		_window.setVisible(true);
		_window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	}

