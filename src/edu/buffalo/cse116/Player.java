package edu.buffalo.cse116;

import java.util.ArrayList;

import javax.swing.JButton;

public class Player 
{

	public int _StartingPosition;
	public int _CurrentSpace;
	public String _CharacterUsed;
	public boolean hasMoved;
	public ArrayList<String> cardsInHand = new ArrayList<String>();
	
	
	//Default Player class constructor.
	public Player()
	{
		
	}
	/**
	 * Specialized Player class constructor.
	 * @param x the suspect that the player would like to be represented by.
	 */
	public Player(String x)
	{
		_CharacterUsed = x;
		setStartingPosition();
		_CurrentSpace = _StartingPosition;	
	}
	/**
	 * sets the name of a player
	 * @param s player name
	 * @return player name
	 */
	public String setName(String s)
	{
		return _CharacterUsed = s;
	}
	/**
	 * Method that sets the starting position on the board depending on the character chosen.
	 */
	public void setStartingPosition(){
		if( _CharacterUsed.equals("Col. Mustard")){
			_StartingPosition = 63;
		}
		if ( _CharacterUsed.equals("Miss Scarlett")){
			_StartingPosition = 2;
		}
		if( _CharacterUsed.equals("Prof. Plum")){
			_StartingPosition = 25;
		}
		if( _CharacterUsed.equals("Rev. Green")){
			_StartingPosition = 187 ;
		}
		if( _CharacterUsed.equals("Mrs. White")){
			_StartingPosition = 188;
		}
		if( _CharacterUsed.equals("Ms. Peacock")){
			_StartingPosition = 154 ;
		}
	}
	/**
	 * Returns the Cards a player has in their hand
	 * @return
	 */
	public ArrayList<String> returnHand()
	{
		return cardsInHand;
	}
	/**
	 * Returns the name of the Character the player is using.
	 * @return
	 */
	public String returnCharacter()
	{
		return _CharacterUsed;
	}
	/**
	 * Returns the space number the player is on.
	 * @return
	 */
	public int getCurrentSpace()
	{
		return _CurrentSpace;
	}
	/**
	 * Sets the player's current space to the value provided by x.
	 * @param x
	 */
	public void setCurrentSpace(int x)
	{
		_CurrentSpace = x;	
	}
	
	public String toString(){
		return _CharacterUsed;
	}
	
}